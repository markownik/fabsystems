﻿namespace VoucherSystem.Model
{
    public class Person : IPerson
    {
        public string Name { get; set; }
        public string Surname { get; set; }

        public override string ToString()
        {
            return $"{Surname} {Name}";
        }
    }

    public interface IPerson
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}