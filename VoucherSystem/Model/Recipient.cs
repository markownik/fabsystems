﻿namespace VoucherSystem.Model
{
    public class Recipient : Person, IRecipient
    {
        public decimal Money { get; set; }
    }

    public interface IRecipient : IPerson
    {
        decimal Money { get; set; }
    }
}