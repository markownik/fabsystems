﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using FabSystems.Shared.Interfaces;
using FabSystems.Shared.Mvvm;
using GalaSoft.MvvmLight.CommandWpf;
using VoucherSystem.Database;
using VoucherSystem.Interfaces;
using VoucherSystem.Model;
using VoucherSystem.Services;

namespace VoucherSystem.ViewModel
{
    public class UpdateVoucherViewModel : BaseVm, IModel<IVoucher>, IVoucher
    {
        private readonly ISettingsService _settingsService;
        private readonly IRepository<VariantDbo> _variantsRepository;
        private readonly IRepository<VoucherDbo> _voucherRepository;
        private VoucherDbo _model;
        private bool _changed;

        public string Title => _model != null ? "Edytuj voucher" : "Dodaj nowy voucher";
        public ICommand SaveCommand => new RelayCommand(Save);
        public ICommand CancelCommand => new RelayCommand(Cancel);

        public string Number { get; set; }
        public DateTime CreationDate { get; set; }
        public int VariantId { get; set; }
        public VariantDbo Variant { get; set; }
        public ObservableCollection<VariantDbo> Variants { get; set; } 
        public string BuyerName { get; set; }
        public string BuyerSurname { get; set; }
        public bool Recipient1SameAsBuyer { get; set; }
        public bool Recipient1Enabled => !Recipient1SameAsBuyer;
        public string Recipient1Name { get; set; }
        public string Recipient1Surname { get; set; }
        public decimal Recipient1Amount { get; set; }
        public bool Recipient2SameAsBuyer { get; set; }
        public bool Recipient2Enabled => !Recipient2SameAsBuyer;
        public string Recipient2Name { get; set; }
        public string Recipient2Surname { get; set; }
        public decimal Recipient2Amount { get; set; }

        public UpdateVoucherViewModel(ISettingsService settingsService, IRepository<VariantDbo> variantsRepository, IRepository<VoucherDbo> voucherRepository)
        {
            _settingsService = settingsService;
            _variantsRepository = variantsRepository;
            _voucherRepository = voucherRepository;
            Variants = new ObservableCollection<VariantDbo>(_variantsRepository.GetAll());
            Recipient1Amount = _settingsService.Amount1;
            Recipient2Amount = _settingsService.Amount2;
        }

        protected override void OnNavigatedTo(params object[] parameters)
        {
            Variants = new ObservableCollection<VariantDbo>(_variantsRepository.GetAll());
            RaisePropertyChanged(() => Variants);
            Recipient1Amount = _settingsService.Amount1;
            Recipient2Amount = _settingsService.Amount2;
            Recipient1SameAsBuyer = false;
            Recipient2SameAsBuyer = false;
            if (parameters.Length == 0 || !(parameters[0] is VoucherDbo updated))
            {
                Clear();
                return;
            }
            _model = updated;
            RaisePropertyChanged(() => Title);
            LoadModel(_model);
            _changed = false;
        }

        public void LoadModel(IVoucher input)
        {
            Number = input.Number;
            CreationDate = input.CreationDate;
            VariantId = input.VariantId;
            if(Variants.All(v => v.Id != VariantId))
                Variants.Add(_variantsRepository.GetById(VariantId));
            Variant = Variants.SingleOrDefault(v => v.Id == VariantId);
            BuyerName = input.BuyerName;
            BuyerSurname = input.BuyerSurname;
            Recipient1Name = input.Recipient1Name;
            Recipient1Surname = input.Recipient1Surname;
            Recipient1Amount = input.Recipient1Amount;
            Recipient2Name = input.Recipient2Name;
            Recipient2Surname = input.Recipient2Surname;
            Recipient2Amount = input.Recipient2Amount;
            Recipient1SameAsBuyer = !string.IsNullOrWhiteSpace($"{Recipient1Name}") && !string.IsNullOrWhiteSpace($"{Recipient1Surname}") && Recipient1Name.Equals(BuyerName) && Recipient1Surname.Equals(BuyerSurname);
            Recipient2SameAsBuyer = !string.IsNullOrWhiteSpace($"{Recipient1Name}") && !string.IsNullOrWhiteSpace($"{Recipient2Surname}") && Recipient2Name.Equals(BuyerName) && Recipient2Surname.Equals(BuyerSurname);

            RaisePropertyChanged(() => Variant);
            RaisePropertyChanged(() => BuyerName);
            RaisePropertyChanged(() => BuyerSurname);
            RaisePropertyChanged(() => Recipient1Name);
            RaisePropertyChanged(() => Recipient1Surname);
            RaisePropertyChanged(() => Recipient1Amount);
            RaisePropertyChanged(() => Recipient2Name);
            RaisePropertyChanged(() => Recipient2Surname);
            RaisePropertyChanged(() => Recipient2Amount);
            RaisePropertyChanged(() => Recipient1SameAsBuyer);
            RaisePropertyChanged(() => Recipient2SameAsBuyer);
            RaisePropertyChanged(() => Recipient1Enabled);
            RaisePropertyChanged(() => Recipient2Enabled);
        }

        public void Clear()
        {
            Number = null;
            CreationDate = DateTime.Now;
            VariantId = -1;
            Variant = null;
            BuyerName = string.Empty;
            BuyerSurname = string.Empty;
            Recipient1Name = string.Empty;
            Recipient1Surname = string.Empty;
            Recipient1Amount = 0;
            Recipient2Name = string.Empty;
            Recipient2Surname = string.Empty;
            Recipient2Amount = 0;
            _model = null;

            RaisePropertyChanged(() => Variant);
            RaisePropertyChanged(() => BuyerName);
            RaisePropertyChanged(() => BuyerSurname);
            RaisePropertyChanged(() => Recipient1Name);
            RaisePropertyChanged(() => Recipient1Surname);
            RaisePropertyChanged(() => Recipient1Amount);
            RaisePropertyChanged(() => Recipient2Name);
            RaisePropertyChanged(() => Recipient2Surname);
            RaisePropertyChanged(() => Recipient2Amount);
            RaisePropertyChanged(() => Recipient1SameAsBuyer);
            RaisePropertyChanged(() => Recipient2SameAsBuyer);
            RaisePropertyChanged(() => Recipient1Enabled);
            RaisePropertyChanged(() => Recipient2Enabled);
        }

        private void Save()
        {
            if (_model == null)
                _model = _voucherRepository.Create();

            _model.Load(this);
            try
            {
                _voucherRepository.Update(_model);

                Clear();
                CurrentWindow?.Close();
            }
            catch(Exception e)
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, e.Message, "UWAGA", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private void Cancel()
        {
            if (_changed)
                if (Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, "Czy na pewno chcesz anulować wprowadzone zmiany?", "UWAGA", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) != MessageBoxResult.Yes)
                    return;

            Clear();
            CurrentWindow?.Close();
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);
            _changed = true;

            if (propertyName == nameof(Variant) && Variant != null)
            {
                VariantId = Variant.Id;
                Recipient1Amount = Variant.Amount1;
                RaisePropertyChanged(() => Recipient1Amount);
                Recipient2Amount = Variant.Amount2;
                RaisePropertyChanged(() => Recipient2Amount);
            }

            if (propertyName == nameof(Recipient1SameAsBuyer))
            {
                RaisePropertyChanged(() => Recipient1Enabled);
                if (Recipient1SameAsBuyer)
                {
                    Recipient1Name = BuyerName;
                    Recipient1Surname = BuyerSurname;
                }
                RaisePropertyChanged(() => Recipient1Name);
                RaisePropertyChanged(() => Recipient1Surname);
            }

            if (propertyName == nameof(Recipient2SameAsBuyer))
            {
                RaisePropertyChanged(() => Recipient2Enabled);
                if (Recipient2SameAsBuyer)
                {
                    Recipient2Name = BuyerName;
                    Recipient2Surname = BuyerSurname;
                }
                RaisePropertyChanged(() => Recipient2Name);
                RaisePropertyChanged(() => Recipient2Surname);
            }
        }
    }
}