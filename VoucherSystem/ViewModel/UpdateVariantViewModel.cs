﻿using System.Windows;
using System.Windows.Input;
using FabSystems.Shared.Interfaces;
using FabSystems.Shared.Mvvm;
using GalaSoft.MvvmLight.CommandWpf;
using VoucherSystem.Database;
using VoucherSystem.Interfaces;
using VoucherSystem.Services;

namespace VoucherSystem.ViewModel
{
    public class UpdateVariantViewModel : BaseVm, IVariant, IModel<IVariant>
    {
        private readonly IRepository<VariantDbo> _varaintsRepository;

        private bool _changed;

        private VariantDbo _model = null;

        public string Title => _model != null ? "Edytuj wariant" : "Dodaj nowy wariant";
        public ICommand SaveCommand => new RelayCommand(Save);
        public ICommand CancelCommand => new RelayCommand(Cancel);

        public string Name { get; set; }
        public decimal Amount1 { get; set; }
        public decimal Amount2 { get; set; }


        public UpdateVariantViewModel(IRepository<VariantDbo> varaintsRepository)
        {
            _varaintsRepository = varaintsRepository;
        }

        protected override void OnNavigatedTo(params object[] parameters)
        {
            if (parameters.Length == 0 || !(parameters[0] is VariantDbo updated))
            {
                Clear();
                return;
            }
            _model = updated;
            RaisePropertyChanged(() => Title);
            LoadModel(_model);
            _changed = false;
        }

        private void Save()
        {
            _model ??= _varaintsRepository.Create();
            _model.Load(this);
            _varaintsRepository.Update(_model);
            
            Clear();
            CurrentWindow?.Close();
        }

        private void Cancel()
        {
            if (_changed)
                if(Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, "Czy na pewno chcesz anulować wprowadzone zmiany?", "UWAGA", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) != MessageBoxResult.Yes)
                    return;

            Clear();
            CurrentWindow?.Close();
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);
            _changed = true;
        }

        public void LoadModel(IVariant input)
        {
            Name = input.Name;
            Amount1 = input.Amount1;
            Amount2 = input.Amount2;

            RaisePropertyChanged(() => Name);
            RaisePropertyChanged(() => Amount1);
            RaisePropertyChanged(() => Amount2);
        }

        public void Clear()
        {
            _model = null;
            Name = string.Empty;
            Amount1 = 0;
            Amount2 = 0;
        }
    }
}