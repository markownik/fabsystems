﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using FabSystems.Shared.Interfaces;
using FabSystems.Shared.Mvvm;
using GalaSoft.MvvmLight.CommandWpf;
using VoucherSystem.Database;
using VoucherSystem.Model;
using VoucherSystem.Services;
using VoucherSystem.Windows;

namespace VoucherSystem.ViewModel
{
    public class VariantsViewModel : BaseVm
    {
        private readonly IRepository<VariantDbo> _variantsRepository;

        public ObservableCollection<VariantDbo> Variants { get; set; } = new ObservableCollection<VariantDbo>();

        public VariantDbo SelectedVariant { get; set; }

        public ICommand AddVariant => new RelayCommand(Add);

        public ICommand EditVariant => new RelayCommand(Edit, () => SelectedVariant != null);

        public ICommand DeleteVariant => new RelayCommand(Delete, () => SelectedVariant != null);

        public VariantsViewModel(IRepository<VariantDbo> variantsRepository)
        {
            _variantsRepository = variantsRepository;
            ReloadData();
        }

        private void ReloadData()
        {
            Variants.Clear();
            foreach (var variant in _variantsRepository.GetAll())
                Variants.Add(variant);
        }

        private void Add()
        {
            OpenDialog<UpdateVariantWindow>();
            ReloadData();
        }

        private void Edit()
        {
            if (SelectedVariant == null)
                return;

            OpenDialog<UpdateVariantWindow>(SelectedVariant);
            ReloadData();
        }

        private void Delete()
        {
            if (SelectedVariant == null)
                return;

            if (Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, "Czy na pewno chcesz usunąć wybrany wariant?", "UWAGA", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
            {
                _variantsRepository.Delete(SelectedVariant);
                SelectedVariant = null;
                ReloadData();
            }
        }
    }
}