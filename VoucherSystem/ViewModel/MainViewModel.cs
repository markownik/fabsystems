using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows.Input;
using FabSystems.Shared.Interfaces;
using FabSystems.Shared.Mvvm;
using FabSystems.Shared.Services;
using GalaSoft.MvvmLight.CommandWpf;
using VoucherSystem.Database;
using VoucherSystem.Model;
using VoucherSystem.Properties;
using VoucherSystem.Services;
using VoucherSystem.Windows;

namespace VoucherSystem.ViewModel
{
    public class MainViewModel : BaseVm
    {
        private readonly ISettingsService _settingsService;
        private readonly ILabelPrinterService _labelPrinterService;
        private readonly VouchersRepository _vouchersRepository;
        private readonly VisitsRepository _visitsRepository;

        public ICommand RefreshPortCommand => new RelayCommand(RefreshComPorts);

        public ICommand RefreshPrinterCommand => new RelayCommand(RefreshPrinters);

        public ICommand SaveCommand => new RelayCommand(SaveSettings);

        public ICommand EditVariants => new RelayCommand(OpenVariantsWindow);

        public ICommand AddVoucherCommand => new RelayCommand(AddVoucherWindow);

        public ICommand EditVoucherCommand => new RelayCommand(EditVoucherWindow, () => SelectedVoucher != null);

        public ICommand SearchVoucherCommand => new RelayCommand(SearchVoucher);

        public ICommand PrintVoucherCommand => new RelayCommand(PrintVoucher, () => SelectedVoucher != null);

        public ICommand AddVisitCommand => new RelayCommand(AddVisitWindow);

        public ICommand EditVisitCommand => new RelayCommand(EditVisitWindow, () => SelectedVisit != null);

        public ObservableCollection<VoucherDbo> Vouchers { get; set; } = new ObservableCollection<VoucherDbo>();

        public VoucherDbo SelectedVoucher { get; set; }

        public ObservableCollection<VisitDbo> Visits { get; set; } = new ObservableCollection<VisitDbo>();

        public VisitDbo SelectedVisit { get; set; }

        public ObservableCollection<string> ComPorts { get; set; } = new ObservableCollection<string>();

        public ObservableCollection<string> Printers { get; set; } = new ObservableCollection<string>();

        public string Printer { get; set; }

        public decimal Amount1Default { get; set; }

        public decimal Amount2Default { get; set; }

        public string ComPort { get; set; }

        public string VoucherSearchString { get; set; }

        public MainViewModel(ISettingsService settingsService, IRepository<VoucherDbo> vouchersRepository, IRepository<VisitDbo> visitsRepository, ILabelPrinterService labelPrinterService)
        {
            _settingsService = settingsService;
            _visitsRepository = visitsRepository as VisitsRepository;
            _labelPrinterService = labelPrinterService;
            _vouchersRepository = vouchersRepository as VouchersRepository;

            Amount1Default = _settingsService.Amount1;
            Amount2Default = _settingsService.Amount2;
            ComPort = _settingsService.ComPort;

            RefreshPrinters();
            RefreshComPorts();
            ReloadVouchers();
            ReloadVisits();
        }

        private void ReloadVouchers(string search = null)
        {
            Vouchers.Clear();
            if (string.IsNullOrWhiteSpace(search))
            {
                foreach (var voucher in _vouchersRepository.Get())
                    Vouchers.Add(voucher);
            }
            else
            {
                foreach (var voucher in _vouchersRepository.Search(VoucherSearchString))
                    Vouchers.Add(voucher);
            }
        }

        private void ReloadVisits()
        {
            Visits.Clear();
            foreach (var visit in _visitsRepository.Get())
                Visits.Add(visit);
        }

        private void RefreshComPorts()
        {
            var temp = ComPort;
            ComPorts.Clear();

            foreach (string port in SerialPort.GetPortNames())
                ComPorts.Add(port);

            
            if (!string.IsNullOrEmpty(temp) && !ComPorts.Contains(temp))
                ComPorts.Add(temp);
            ComPort = temp;

            RaisePropertyChanged(() => ComPorts);
            RaisePropertyChanged(() => ComPort);
        }

        private void RefreshPrinters()
        {
            var temp = Printer;
            Printers.Clear();

            foreach (string printer in _labelPrinterService.Printers)
                Printers.Add(printer);

            if (!string.IsNullOrEmpty(temp) && !Printers.Contains(temp))
                Printers.Add(temp);

            Printer = temp;

            _labelPrinterService.SelectedPrinter = Printer;
            RaisePropertyChanged(() => Printers);
            RaisePropertyChanged(() => Printer);
        }

        private void SaveSettings()
        {
            _labelPrinterService.SelectedPrinter = Printer;
            _settingsService.Printer = Printer;
            _settingsService.ComPort = ComPort;
            _settingsService.Amount1 = Amount1Default;
            _settingsService.Amount2 = Amount2Default;
        }

        private void OpenVariantsWindow()
        {
            OpenWindow<VariantsWindow>();
        }

        private void AddVoucherWindow()
        {
            OpenDialog<UpdateVoucherWindow>();
            ReloadVouchers(VoucherSearchString);
            SelectedVoucher = Vouchers.OrderByDescending(v => v.CreationDate).FirstOrDefault();
            RaisePropertyChanged(nameof(SelectedVoucher));
        }

        private void EditVoucherWindow()
        {
            var temp = SelectedVoucher.Id;
            OpenDialog<UpdateVoucherWindow>(SelectedVoucher);
            ReloadVouchers(VoucherSearchString);
            SelectedVoucher = Vouchers.Single(v => v.Id == temp);
            RaisePropertyChanged(nameof(SelectedVoucher));
        }

        private void AddVisitWindow()
        {
            OpenDialog<UpdateVisitWindow>();
            ReloadVisits();
            SelectedVisit = Visits.OrderByDescending(v => v.Date).FirstOrDefault();
            RaisePropertyChanged(nameof(SelectedVisit));
        }

        private void EditVisitWindow()
        {
            var temp = SelectedVisit.Id;
            OpenDialog<UpdateVisitWindow>(SelectedVisit);
            ReloadVisits();
            SelectedVisit = Visits.Single(v => v.Id == temp);
            RaisePropertyChanged(nameof(SelectedVisit));
        }

        private void SearchVoucher()
        {
            ReloadVouchers(VoucherSearchString);
        }

        private void PrintVoucher()
        {
            _labelPrinterService.SelectedPrinter = _settingsService.Printer;
            _labelPrinterService.PrintVoucherLabel(SelectedVoucher.Number, SelectedVoucher.CreationDate, Encoding.UTF8.GetString(Resources.voucher));
        }
    }
}