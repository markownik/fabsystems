/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:VoucherSystem"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using System.Diagnostics;
using CommonServiceLocator;
using FabSystems.Shared.Interfaces;
using FabSystems.Shared.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using VoucherSystem.Database;
using VoucherSystem.Model;
using VoucherSystem.Services;

namespace VoucherSystem.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (!ViewModelBase.IsInDesignModeStatic)
            {
                SimpleIoc.Default.Register<ISettingsService>(() => new SettingsService(), true);
                SimpleIoc.Default.Register<IChecksumService>(() => new ChecksumService());
                SimpleIoc.Default.Register<ILabelPrinterService>(() => new LabelPrinterService());
                SimpleIoc.Default.Register<IScannerService>(() => new ScannerService());
                SimpleIoc.Default.Register<IRepository<VariantDbo>>(() => new VariantsRepository());
                SimpleIoc.Default.Register<IRepository<VoucherDbo>>(() => new VouchersRepository(SimpleIoc.Default.GetInstance<IChecksumService>()));
                SimpleIoc.Default.Register<IRepository<VisitDbo>>(() => new VisitsRepository());
            }

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<VariantsViewModel>();
            SimpleIoc.Default.Register<UpdateVariantViewModel>();
            SimpleIoc.Default.Register<UpdateVoucherViewModel>();
            SimpleIoc.Default.Register<UpdateVisitViewModel>();
        }

        public MainViewModel Main => ServiceLocator.Current.GetInstance<MainViewModel>();

        public VariantsViewModel Variants => ServiceLocator.Current.GetInstance<VariantsViewModel>();

        public UpdateVariantViewModel UpdateVariant => ServiceLocator.Current.GetInstance<UpdateVariantViewModel>();

        public UpdateVoucherViewModel UpdateVoucher => ServiceLocator.Current.GetInstance<UpdateVoucherViewModel>();

        public UpdateVisitViewModel UpdateVisit => ServiceLocator.Current.GetInstance<UpdateVisitViewModel>();

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}