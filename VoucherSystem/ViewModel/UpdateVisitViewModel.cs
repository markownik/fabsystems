﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;
using FabSystems.Shared.Interfaces;
using FabSystems.Shared.Mvvm;
using FabSystems.Shared.Services;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Threading;
using VoucherSystem.Database;
using VoucherSystem.Interfaces;
using VoucherSystem.Services;

namespace VoucherSystem.ViewModel
{
    public class UpdateVisitViewModel : BaseVm, IVisit, IModel<IVisit>
    {
        private readonly IChecksumService _checksumService;
        private readonly IScannerService _scannerService;
        private readonly ISettingsService _settingsService;
        private readonly VouchersRepository _vouchersRepository;
        private readonly VisitsRepository _visitsRepository;
        private bool _changed;
        private VisitDbo _model = null;

        public string Title => _model != null ? "Edytuj wizytę" : "Dodaj nową wizytę";
        public ICommand SaveCommand => new RelayCommand(Save);
        public ICommand CancelCommand => new RelayCommand(Cancel);
        public ICommand OpenVoucherSearch => new RelayCommand(OpenSearchVoucherWindow, () => false);

        public string VoucherNumber => Voucher?.Number ?? "zeskanuj lub wyszukaj";
        public decimal Amount1Left => Voucher?.Recipient1Amount - (Recipient1Amount + Voucher?.Visits?.Where(v => _model == null || v.Id != _model.Id).Sum(v => v.Recipient1Amount)) ?? 0;
        public decimal Amount2Left => Voucher?.Recipient2Amount - (Recipient2Amount + Voucher?.Visits?.Where(v => _model == null || v.Id != _model.Id).Sum(v => v.Recipient2Amount)) ?? 0;
        public string Recipient1 => Voucher != null ? $"{Voucher.Recipient1Name} {Voucher.Recipient1Surname}" : "brak";
        public string Recipient2 => Voucher != null ? $"{Voucher.Recipient2Name} {Voucher.Recipient2Surname}" : "brak";


        public decimal Amount1LeftFixed { get; set; }
        public decimal Amount2LeftFixed { get; set; }

        public int VoucherId { get; set; }
        public VoucherDbo Voucher { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
        public decimal Recipient1Amount { get; set; }
        public decimal Recipient2Amount { get; set; }
        public string ProcedurePerson { get; set; }

        public UpdateVisitViewModel(IRepository<VoucherDbo> vouchersRepository, IRepository<VisitDbo> visitsRepository, IChecksumService checksumService, IScannerService scannerService, ISettingsService settingsService)
        {
            _checksumService = checksumService;
            _scannerService = scannerService;
            _settingsService = settingsService;
            _vouchersRepository = vouchersRepository as VouchersRepository;
            _visitsRepository = visitsRepository as VisitsRepository;
        }

        protected override void OnNavigatedTo(params object[] parameters)
        {
            MessengerInstance.Register<CodeScannedMessage>(this, CodeScannedMessageReceived);
            _scannerService.Start(_settingsService.ComPort);
            if (parameters.Length == 0 || !(parameters[0] is VisitDbo updated))
            {
                Clear();
                return;
            }
               
            _model = updated;
            RaisePropertyChanged(() => Title);
            LoadModel(_model);
            _changed = false;
        }

        protected override void OnNavigatedFrom()
        {
            MessengerInstance.Unregister(this);
            _scannerService.Stop();
        }

        private void CodeScannedMessageReceived(CodeScannedMessage msg)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                try
                {
                    var number = msg.Content;
                    var numberT = number.Split('-');
                    if (!_checksumService.ValidateChecksum($"{numberT[0]}-{numberT[1]}", numberT[2]))
                        throw new FormatException();

                    try
                    {
                        Voucher = _vouchersRepository.SearchByNumber(number).Single();
                        VoucherId = Voucher.Id;
                        Amount1LeftFixed = Amount1Left;
                        Amount2LeftFixed = Amount2Left;

                        RaisePropertyChanged(nameof(VoucherNumber));
                        RaisePropertyChanged(nameof(Amount1Left));
                        RaisePropertyChanged(nameof(Amount2Left));
                        RaisePropertyChanged(nameof(Amount1LeftFixed));
                        RaisePropertyChanged(nameof(Amount2LeftFixed));
                        RaisePropertyChanged(nameof(Recipient1));
                        RaisePropertyChanged(nameof(Recipient2));
                    }
                    catch (Exception)
                    {
                        Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, $"Voucher o numerze {number} nie istnieje", "UWAGA",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch
                {
                    Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, "Numer posiada zły format", "UWAGA",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
            });
        }

        private void Save()
        {
            if (VoucherId > 0)
            {
                try
                {
                    if (_vouchersRepository.GetById(VoucherId) == null)
                        throw new NullReferenceException();
                }
                catch
                {
                    Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, "Nie wybrano vouchera do wizyty!", "UWAGA", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            else
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, "Nie wybrano vouchera do wizyty!", "UWAGA", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            _model ??= _visitsRepository.Create();
            _model.Load(this);

            try
            {
                _visitsRepository.Update(_model);
                Clear();
                CurrentWindow?.Close();
            }
            catch (Exception e)
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, e.Message, "UWAGA", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private void Cancel()
        {
            if (_changed)
                if (Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, "Czy na pewno chcesz anulować wprowadzone zmiany?", "UWAGA", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) != MessageBoxResult.Yes)
                    return;

            Clear();
            CurrentWindow?.Close();
        }

        private void OpenSearchVoucherWindow()
        {

        }

        public void LoadModel(IVisit input)
        {
            VoucherId = input.VoucherId;
            Voucher = input.Voucher;
            Date = input.Date;
            Recipient1Amount = input.Recipient1Amount;
            Recipient2Amount = input.Recipient2Amount;
            ProcedurePerson = input.ProcedurePerson;
            Amount1LeftFixed = Amount1Left;
            Amount2LeftFixed = Amount2Left;

            RaisePropertyChanged(nameof(VoucherId));
            RaisePropertyChanged(nameof(Voucher));
            RaisePropertyChanged(nameof(Date));
            RaisePropertyChanged(nameof(Recipient1Amount));
            RaisePropertyChanged(nameof(Recipient2Amount));
            RaisePropertyChanged(nameof(ProcedurePerson));
            RaisePropertyChanged(nameof(VoucherNumber));
            RaisePropertyChanged(nameof(Amount1Left));
            RaisePropertyChanged(nameof(Amount2Left));
            RaisePropertyChanged(nameof(Amount1LeftFixed));
            RaisePropertyChanged(nameof(Amount2LeftFixed));
            RaisePropertyChanged(nameof(Recipient1));
            RaisePropertyChanged(nameof(Recipient2));
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == nameof(Recipient1Amount) || propertyName == nameof(Recipient2Amount))
            {
                RaisePropertyChanged(nameof(Amount1Left));
                RaisePropertyChanged(nameof(Amount2Left));
            }
            _changed = true;
        }

        public void Clear()
        {
            _model = null;
            Voucher = null;
            VoucherId = -1;
            Date = DateTime.Now;
            Recipient1Amount = 0;
            Recipient2Amount = 0;
            ProcedurePerson = string.Empty;
            Amount1LeftFixed = 0;
            Amount2LeftFixed = 0;
        }
    }
}