﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SQLite.CodeFirst;

namespace VoucherSystem.Database
{
    [Table("settings")]
    public class SettingsDbo
    {
        [Column("id"), Key, Unique]
        public int Id { get; set; }

        [Column("amount1_default")]
        public decimal Amount1Default { get; set; }

        [Column("amount2_default")]
        public decimal Amount2Default { get; set; }

        [Column("reader_com_port")]
        public string ReaderComPort { get; set; }

        [Column("printer_name")]
        public string PrinterName { get; set; }
    }
}