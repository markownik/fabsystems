﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SQLite.CodeFirst;
using VoucherSystem.Interfaces;

namespace VoucherSystem.Database
{
    [Table("variants")]
    public class VariantDbo : IVariant
    {
        [Column("id"), Key, Index, Unique, Autoincrement]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("amount_1")]
        public decimal Amount1 { get; set; }

        [Column("amount_2")]
        public decimal Amount2 { get; set; }

        [Column("active")] 
        public bool Active { get; set; } = true;

        public void Load(IVariant value)
        {
            Name = value.Name;
            Amount1 = value.Amount1;
            Amount2 = value.Amount2;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}