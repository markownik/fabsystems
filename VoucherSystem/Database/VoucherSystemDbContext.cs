﻿using System.Data.Entity;
using SQLite.CodeFirst;

namespace VoucherSystem.Database
{
    public class VoucherSystemDbContext : DbContext
    {
        public VoucherSystemDbContext() : base("VSDB") { }
        
        public VoucherSystemDbContext(string dbConnectionString) : base(dbConnectionString) { }

        public DbSet<VoucherDbo> Vouchers { get; set; }

        public DbSet<VisitDbo> Visits { get; set; }

        public DbSet<VariantDbo> Variants { get; set; }

        public DbSet<SettingsDbo> Settings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            System.Data.Entity.Database.SetInitializer(new SqliteCreateDatabaseIfNotExists<VoucherSystemDbContext>(modelBuilder));
        }
    }
}