﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using SQLite.CodeFirst;
using VoucherSystem.Interfaces;
using VoucherSystem.Model;

namespace VoucherSystem.Database
{
    [Table("vouchers")]
    public class VoucherDbo : IVoucher
    {
        [Column("id"), Key, Index, Unique, Autoincrement]
        public int Id { get; set; }

        [Column("number")]
        public string Number { get; set; }

        [Column("creation_date")]
        public DateTime CreationDate { get; set; }

        [Column("variant_id"), ForeignKey(nameof(Variant))]
        public int VariantId { get; set; }

        public VariantDbo Variant { get; set; }

        [Column("buyer_name")]
        public string BuyerName { get; set; }

        [Column("buyer_surname")]
        public string BuyerSurname { get; set; }

        [Column("recipient1_name")]
        public string Recipient1Name { get; set; }

        [Column("recipient1_surname")]
        public string Recipient1Surname { get; set; }

        [Column("recipient1_amount")]
        public decimal Recipient1Amount { get; set; }

        [Column("recipient2_name")]
        public string Recipient2Name { get; set; }

        [Column("recipient2_surname")]
        public string Recipient2Surname { get; set; }

        [Column("recipient2_amount")]
        public decimal Recipient2Amount { get; set; }

        public ICollection<VisitDbo> Visits { get; set; }

        public void Load(IVoucher value)
        {
            VariantId = value.VariantId;
            BuyerName = value.BuyerName;
            BuyerSurname = value.BuyerSurname;
            Recipient1Name = value.Recipient1Name;
            Recipient1Surname = value.Recipient1Surname;
            Recipient1Amount = value.Recipient1Amount;
            Recipient2Name = value.Recipient2Name;
            Recipient2Surname = value.Recipient2Surname;
            Recipient2Amount = value.Recipient2Amount;
        }
    }
}