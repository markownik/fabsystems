﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SQLite.CodeFirst;
using VoucherSystem.Interfaces;

namespace VoucherSystem.Database
{
    [Table("visits")]
    public class VisitDbo : IVisit
    {
        [Column("id"), Key, Index, Unique, Autoincrement]
        public int Id { get; set; }

        [Column("voucher_id"), ForeignKey(nameof(Voucher))]
        public int VoucherId { get; set; }

        public VoucherDbo Voucher { get; set; }

        [Column("visit_date")] 
        public DateTime Date { get; set; }

        [Column("recipient1_amount")] 
        public decimal Recipient1Amount { get; set; }

        [Column("recipient2_amount")] 
        public decimal Recipient2Amount { get; set; }

        [Column("procedure_person")] 
        public string ProcedurePerson { get; set; }

        public void Load(IVisit value)
        {
            VoucherId = value.VoucherId;
            Date = value.Date;
            Recipient1Amount = value.Recipient1Amount;
            Recipient2Amount = value.Recipient2Amount;
            ProcedurePerson = value.ProcedurePerson;
        }
    }
}