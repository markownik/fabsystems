﻿namespace VoucherSystem.Interfaces
{
    public interface IVariant
    {
        public string Name { get; set; }

        public decimal Amount1 { get; set; }

        public decimal Amount2 { get; set; }
    }
}