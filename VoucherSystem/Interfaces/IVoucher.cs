﻿using System;
using VoucherSystem.Database;
using VoucherSystem.Model;

namespace VoucherSystem.Interfaces
{
    public interface IVoucher
    {
        public string Number { get; set; }

        public DateTime CreationDate { get; set; }

        public int VariantId { get; set; }

        public VariantDbo Variant { get; set; }

        public string BuyerName { get; set; }

        public string BuyerSurname { get; set; }

        public string Recipient1Name { get; set; }

        public string Recipient1Surname { get; set; }

        public decimal Recipient1Amount { get; set; }

        public string Recipient2Name { get; set; }

        public string Recipient2Surname { get; set; }

        public decimal Recipient2Amount { get; set; }

    }
}