﻿using System;
using VoucherSystem.Database;

namespace VoucherSystem.Interfaces
{
    public interface IVisit
    {
        public int VoucherId { get; set; }

        public VoucherDbo Voucher { get; set; }

        public DateTime Date { get; set; }

        public decimal Recipient1Amount { get; set; }

        public decimal Recipient2Amount { get; set; }

        public string ProcedurePerson { get; set; }
    }
}