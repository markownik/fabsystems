﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using FabSystems.Shared.Interfaces;
using FabSystems.Shared.Services;
using VoucherSystem.Database;
using VoucherSystem.Model;

namespace VoucherSystem.Services
{
    public class VouchersRepository : IRepository<VoucherDbo>
    {
        private readonly IChecksumService _checksumService;

        public VouchersRepository(IChecksumService checksumService)
        {
            _checksumService = checksumService;
        }

        public IEnumerable<VoucherDbo> GetAll()
        {
            using var db = new VoucherSystemDbContext();
            IEnumerable<VoucherDbo> result = db.Vouchers.ToList();

            return result;
        }

        public IEnumerable<VoucherDbo> Search(string phrase)
        {
            using var db = new VoucherSystemDbContext();
            IEnumerable<VoucherDbo> result = db.Vouchers.Where(voucher => voucher.Number.Contains(phrase) 
                                                  || (voucher.BuyerName + " " + voucher.BuyerSurname).Contains(phrase)
                                                  || (voucher.Recipient1Name + " " + voucher.Recipient1Surname).Contains(phrase)
                                                  || (voucher.Recipient2Name + " " + voucher.Recipient2Surname).Contains(phrase))
                                                    .Take(100).Include(voucher => voucher.Variant).ToList();

            return result;
        }

        public IEnumerable<VoucherDbo> SearchByNumber(string number)
        {
            using var db = new VoucherSystemDbContext();
            IEnumerable<VoucherDbo> result = db.Vouchers.Where(voucher => voucher.Number.Contains(number)).Take(10).Include(voucher => voucher.Variant).Include(voucher => voucher.Visits).ToList();

            return result;
        }

        public IEnumerable<VoucherDbo> Get(int count = 100)
        {
            using var db = new VoucherSystemDbContext();
            IEnumerable<VoucherDbo> result = db.Vouchers.OrderByDescending(voucher => voucher.CreationDate).Take(count).Include(voucher => voucher.Variant).ToList();

            return result;
        }

        public VoucherDbo GetById(int id)
        {
            using var db = new VoucherSystemDbContext();
            var result = db.Vouchers.Where(voucher => voucher.Id == id).Include(voucher => voucher.Variant).Include(voucher => voucher.Visits).SingleOrDefault();

            return result;
        }

        public void Update(VoucherDbo element)
        {
            using var db = new VoucherSystemDbContext();
            if (db.Visits.Any(visit => visit.VoucherId == element.Id))
                throw new Exception("Voucher posiada już przypisane wizyty!");
            db.Vouchers.AddOrUpdate(element);
            db.SaveChanges();
        }

        public bool Delete(VoucherDbo element)
        {
            using var db = new VoucherSystemDbContext();
            if(GetById(element.Id).Visits.Any())
                return false;

            db.Vouchers.Remove(element);
            db.SaveChanges();
            return true;
        }

        public VoucherDbo Create()
        {
            using var db = new VoucherSystemDbContext();
            var result = db.Vouchers.Create();

            int count = db.Vouchers.Count(voucher => voucher.CreationDate >= DateTime.Today);
            result.CreationDate = DateTime.Now;
            result.Number = $"{result.CreationDate:yyyyMMdd}-{count+1:D4}";
            var checksum = _checksumService.GetChecksum(result.Number);
            result.Number = $"{result.Number}-{checksum}";
                
            db.SaveChanges();
            return result;
        }
    }
}