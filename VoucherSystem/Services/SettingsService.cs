﻿using System;
using System.Data.Entity.Migrations;
using System.Data.Odbc;
using System.Diagnostics;
using System.Linq;
using VoucherSystem.Database;

namespace VoucherSystem.Services
{
    public interface ISettings
    {
        decimal Amount1 { get; set; }
        decimal Amount2 { get; set; }
        string ComPort { get; set; }
        string Printer { get; set; }
    }

    public interface ISettingsService : ISettings
    {
    }

    public class SettingsService : ISettingsService
    {
        public decimal Amount1
        {
            get => GetSetting<decimal>(nameof(SettingsDbo.Amount1Default));
            set => SetSetting(nameof(SettingsDbo.Amount1Default), value);
        }

        public decimal Amount2
        {
            get => GetSetting<decimal>(nameof(SettingsDbo.Amount2Default));
            set => SetSetting(nameof(SettingsDbo.Amount2Default), value);
        }

        public string ComPort
        {
            get => GetSetting<string>(nameof(SettingsDbo.ReaderComPort));
            set => SetSetting(nameof(SettingsDbo.ReaderComPort), value);
        }

        public string Printer
        {
            get => GetSetting<string>(nameof(SettingsDbo.PrinterName));
            set => SetSetting(nameof(SettingsDbo.PrinterName), value);
        }

        private T GetSetting<T>(string name)
        {
            using (var db = new VoucherSystemDbContext())
            {
                try
                {
                    return (T) typeof(SettingsDbo).GetProperty(name)?.GetValue(db.Settings.SingleOrDefault(s => s.Id == 1));
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                    return default;
                }
            }
        }

        private void SetSetting<T>(string name, T value)
        {
            using (var db = new VoucherSystemDbContext())
            {
                try
                {
                    SettingsDbo setting;
                    if (!db.Settings.Any())
                        setting = db.Settings.Create();
                    else
                        setting = db.Settings.SingleOrDefault(s => s.Id == 1);

                    if(value != null)
                        setting?.GetType()?.GetProperty(name)?.SetValue(setting, value);

                    db.Settings.AddOrUpdate(setting);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                }
            }
        }

        public SettingsService()
        {
            try
            {
                using (var db = new VoucherSystemDbContext())
                {
                    db.Database.Initialize(false);
                    Debug.WriteLine($"db: {db.Database.Connection.ConnectionString}");
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Could not initialize db: {e.Message}");
                throw;
            }
        }
    }
}