﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using FabSystems.Shared.Interfaces;
using VoucherSystem.Database;

namespace VoucherSystem.Services
{
    public class VariantsRepository : IRepository<VariantDbo>
    {
        public IEnumerable<VariantDbo> GetAll()
        {
            using var db = new VoucherSystemDbContext();
            IEnumerable<VariantDbo> result = db.Variants.Where(variant => variant.Active).ToList();
            return result;
        }

        public VariantDbo GetById(int id)
        {
            using var db = new VoucherSystemDbContext();
            var result = db.Variants.SingleOrDefault(variant => variant.Id == id);
            return result;
        }

        public void Update(VariantDbo element)
        {
            using var db = new VoucherSystemDbContext();
            db.Variants.AddOrUpdate(element);
            db.SaveChanges();
        }

        public bool Delete(VariantDbo element)
        {
            element.Active = false;

            using var db = new VoucherSystemDbContext();
            db.Variants.AddOrUpdate(element);
            db.SaveChanges();

            return true;
        }

        public VariantDbo Create()
        {
            using var db = new VoucherSystemDbContext();
            var result = db.Variants.Create();
            result.Active = true;
            db.SaveChanges();
            return result;
        }
    }
}