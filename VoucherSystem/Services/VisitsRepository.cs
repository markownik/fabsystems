﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using FabSystems.Shared.Interfaces;
using VoucherSystem.Database;

namespace VoucherSystem.Services
{
    public class VisitsRepository : IRepository<VisitDbo>
    {
        public IEnumerable<VisitDbo> GetAll()
        {
            using var db = new VoucherSystemDbContext();
            IEnumerable<VisitDbo> result = db.Visits.ToList();
            return result;
        }

        public VisitDbo GetById(int id)
        {
            using var db = new VoucherSystemDbContext();
            var result = db.Visits.Where(visit => visit.Id == id).Include(visit => visit.Voucher).SingleOrDefault();
            return result;
        }

        public IEnumerable<VisitDbo> Search(string phrase)
        {
            using var db = new VoucherSystemDbContext();
            IEnumerable<VisitDbo> result = db.Visits.Include(visit => visit.Voucher)
                                                    .Where(visit => visit.ProcedurePerson.Contains(phrase) 
                                                        || (visit.Voucher.BuyerName + " " + visit.Voucher.BuyerSurname).Contains(phrase) 
                                                        || (visit.Voucher.Recipient1Name + " " + visit.Voucher.Recipient1Surname).Contains(phrase) 
                                                        || (visit.Voucher.Recipient2Name + " " + visit.Voucher.Recipient2Surname).Contains(phrase))
                                                    .Take(100).ToList();

            return result;
        }

        public IEnumerable<VisitDbo> Get(int count = 100)
        {
            using var db = new VoucherSystemDbContext();
            IEnumerable<VisitDbo> result = db.Visits.OrderByDescending(visit => visit.Date).Take(count).Include(visit => visit.Voucher).ToList();

            return result;
        }

        public void Update(VisitDbo element)
        {
            using var db = new VoucherSystemDbContext();
            db.Visits.AddOrUpdate(element);
            db.SaveChanges();
        }

        public bool Delete(VisitDbo element)
        {
            using var db = new VoucherSystemDbContext();
            db.Visits.Remove(element);
            db.SaveChanges();
            return true;
        }

        public VisitDbo Create()
        {
            using var db = new VoucherSystemDbContext();
            var result = db.Visits.Create();
            db.SaveChanges();
            return result;
        }
    }
}