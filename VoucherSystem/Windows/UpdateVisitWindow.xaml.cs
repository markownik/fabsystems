﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FabSystems.Shared.Mvvm;
using VoucherSystem.ViewModel;
using Xceed.Wpf.ListBox;

namespace VoucherSystem.Windows
{
    /// <summary>
    /// Interaction logic for UpdateVisitWindow.xaml
    /// </summary>
    public partial class UpdateVisitWindow : Window
    {
        private BaseVm ViewModel => (DataContext as BaseVm);

        public UpdateVisitWindow()
        {
            InitializeComponent();
            ViewModel.CurrentWindow = this;
        }
    }
}
