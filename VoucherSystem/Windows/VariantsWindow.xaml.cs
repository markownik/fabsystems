﻿using System.Windows;
using FabSystems.Shared.Mvvm;
using VoucherSystem.ViewModel;

namespace VoucherSystem.Windows
{
    /// <summary>
    /// Interaction logic for VariantsWindow.xaml
    /// </summary>
    public partial class VariantsWindow : Window
    {
        private BaseVm ViewModel => (DataContext as BaseVm);

        public VariantsWindow()
        {
            InitializeComponent();
            ViewModel.CurrentWindow = this;
        }
    }
}
