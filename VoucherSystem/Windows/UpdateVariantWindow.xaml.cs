﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FabSystems.Shared.Mvvm;
using VoucherSystem.ViewModel;

namespace VoucherSystem.Windows
{
    /// <summary>
    /// Interaction logic for UpdateVariantWindow.xaml
    /// </summary>
    public partial class UpdateVariantWindow : Window
    {
        private BaseVm ViewModel => (DataContext as BaseVm);

        public UpdateVariantWindow()
        {
            InitializeComponent();
            ViewModel.CurrentWindow = this;
        }
    }
}
