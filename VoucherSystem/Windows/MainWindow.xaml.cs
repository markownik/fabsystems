﻿using System.Diagnostics;
using System.Globalization;
using System.Windows;
using FabSystems.Shared.Mvvm;
using VoucherSystem.ViewModel;

namespace VoucherSystem.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BaseVm ViewModel => (DataContext as BaseVm);

        public MainWindow()
        {
            InitializeComponent();
            ViewModel.CurrentWindow = this;

            Debug.WriteLine($"Current culture: {CultureInfo.CurrentCulture.Name}");
        }
    }
}
