﻿using System;
using System.ComponentModel;
using System.Windows;
using GalaSoft.MvvmLight;

namespace FabSystems.Shared.Mvvm
{
    public class BaseVm : ViewModelBase
    {
        public Window CurrentWindow { get; set; }

        protected BaseVm()
        {
            this.PropertyChanged += delegate(object sender, PropertyChangedEventArgs args) { OnPropertyChanged(args.PropertyName); };
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
        }

        protected virtual void OnNavigatedTo(params object[] parameters)
        {
        }

        protected virtual void OnNavigatedFrom()
        {
        }

        protected void OpenWindow<T>(params object[] navigationParameters) where T : Window
        {
            var window = Activator.CreateInstance<T>();
            window.Owner = CurrentWindow;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            if (navigationParameters != null)
                (window.DataContext as BaseVm)?.OnNavigatedTo(navigationParameters);
            else
                (window.DataContext as BaseVm)?.OnNavigatedTo();
            window.Closing += delegate { (window.DataContext as BaseVm)?.OnNavigatedFrom(); };
            window.Show();
        }

        protected void OpenDialog<T>(params object[] navigationParameters) where T : Window
        {
            var window = Activator.CreateInstance<T>();
            window.Owner = CurrentWindow;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            if (navigationParameters != null)
                (window.DataContext as BaseVm)?.OnNavigatedTo(navigationParameters);
            else
                (window.DataContext as BaseVm)?.OnNavigatedTo();
            window.Closing += delegate { (window.DataContext as BaseVm)?.OnNavigatedFrom(); };
            window.ShowDialog();
        }
    }
}