﻿namespace FabSystems.Shared.Extensions
{
    public static class ByteExtensions
    {
        public static string ToHexString(this byte[] array)
        {
            string result = string.Empty;
            foreach (var b in array)
                result = $"{result}{b:X2}";
            return result;
        }
    }
}