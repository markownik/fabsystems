﻿using System.Collections.Generic;

namespace FabSystems.Shared.Interfaces
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();

        T GetById(int id);

        void Update(T element);

        bool Delete(T element);

        T Create();
    }
}