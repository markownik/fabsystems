﻿namespace FabSystems.Shared.Interfaces
{
    public interface IModel<T>
    {
        void LoadModel(T input);

        void Clear();
    }
}