﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using DymoSDK.Implementations;
using DymoSDK.Interfaces;
using GalaSoft.MvvmLight.Threading;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace FabSystems.Shared.Services
{
    public class LabelPrinterService : ILabelPrinterService
    {
        public List<string> Printers => DymoPrinter.Instance.GetPrinters().Select(p => p.Name).ToList();

        public string SelectedPrinter { get; set; }

        private IPrinter Printer => string.IsNullOrWhiteSpace(SelectedPrinter) ? DymoPrinter.Instance.GetPrinters().FirstOrDefault() : DymoPrinter.Instance.GetPrinters().SingleOrDefault(p => p.Name == SelectedPrinter);

        public void PrintVoucherLabel(string voucherNumber, DateTime voucherCreationDate, string labelXml)
        {
            new Task(() =>
            {
                Debug.WriteLine($"printing voucher: {voucherNumber}");
                try
                {
                    if (Printer == null)
                    {
                        DispatcherHelper.CheckBeginInvokeOnUI(() =>
                        {
                            MessageBox.Show("Nie wykryto drukarki DYMO w systemie, czy na pewno została zainstalowana?",
                                "UWAGA",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                        });
                        return;
                    }

                    if (!Printer.IsConnected)
                    {
                        DispatcherHelper.CheckBeginInvokeOnUI(() =>
                        {
                            MessageBox.Show("Drukarka DYMO jest odłączona, podłącz drukarkę i spróbuj ponownie",
                                "UWAGA",
                                MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        });
                        return;
                    }

                    var label = new DymoLabel();
                    label.LoadLabelFromXML(labelXml);

                    var number = voucherNumber.Split('-');
                    label.UpdateLabelObject(label.GetLabelObject("TEXT_INFO"), $"{number[0]}-{number[1]}\n{voucherCreationDate:HH:mm:ss}");
                    label.UpdateLabelObject(label.GetLabelObject("QR_CODE"), voucherNumber);

                    DymoPrinter.Instance.PrintLabel(label, Printer.Name, barcodeGraphsQuality: true);
                }
                catch (Exception e)
                {
                    DispatcherHelper.CheckBeginInvokeOnUI(() =>
                    {
                        MessageBox.Show($"Wystąpił krytyczny błąd: {e.Message}",
                            "UWAGA",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                    });
                }
            }).Start();
        }

        public void PrintProductBarcode(string barcode, string name, string labelXml, int count = 1)
        {
            new Task(() =>
            {
                Debug.WriteLine($"printing barcode: {name}, {count} pcs");
                try
                {
                    if (Printer == null)
                    {
                        DispatcherHelper.CheckBeginInvokeOnUI(() =>
                        {
                            MessageBox.Show("Nie wykryto drukarki DYMO w systemie, czy na pewno została zainstalowana?",
                                "UWAGA",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                        });
                        return;
                    }

                    if (!Printer.IsConnected)
                    {
                        DispatcherHelper.CheckBeginInvokeOnUI(() =>
                        {
                            MessageBox.Show("Drukarka DYMO jest odłączona, podłącz drukarkę i spróbuj ponownie",
                                "UWAGA",
                                MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        });
                        return;
                    }

                    var label = new DymoLabel();
                    label.LoadLabelFromXML(labelXml);

                    label.UpdateLabelObject(label.GetLabelObject("TEXT_INFO"), $"{name}");
                    label.UpdateLabelObject(label.GetLabelObject("QR_CODE"), barcode);

                    while (count-- > 0)
                        DymoPrinter.Instance.PrintLabel(label, Printer.Name, barcodeGraphsQuality: true);
                }
                catch (Exception e)
                {
                    DispatcherHelper.CheckBeginInvokeOnUI(() =>
                    {
                        MessageBox.Show($"Wystąpił krytyczny błąd: {e.Message}",
                            "UWAGA",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                    });
                }
            }).Start();
        }
    }

    public interface ILabelPrinterService
    {
        List<string> Printers { get; }

        string SelectedPrinter { get; set; }

        void PrintVoucherLabel(string voucherNumber, DateTime voucherCreationDate, string labelXml);

        void PrintProductBarcode(string barcode, string productName, string labelXml, int count = 1);
    }
}