﻿using System.Security.Cryptography;
using System.Text;
using FabSystems.Shared.Extensions;

namespace FabSystems.Shared.Services
{
    public class ChecksumService : IChecksumService
    {
        public string GetChecksum(string input)
        {
            string result;
            using (SHA256 digest = SHA256.Create())
            {
                result = digest.ComputeHash(Encoding.UTF8.GetBytes(input)).ToHexString();
            }
            return result.Substring(0, 8);
        }

        public bool ValidateChecksum(string input, string checksum)
        {
            return checksum.Equals(GetChecksum(input));
        }
    }

    public interface IChecksumService
    {
        string GetChecksum(string input);

        bool ValidateChecksum(string input, string checksum);
    }
}