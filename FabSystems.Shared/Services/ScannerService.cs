﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace FabSystems.Shared.Services
{
    public class ScannerService : IScannerService
    {
        private Task _scanTask;
        private volatile SerialPort _serial;
        private CancellationTokenSource _cancellationToken;

        public ScannerService()
        {
        }

        public void Start(string port, bool silent = false)
        {
            try
            {
                Stop();
                
                _scanTask = new Task(() =>
                {
                    try
                    {
                        _serial = new SerialPort(port)
                        {
                            BaudRate = 115200,
                            DataBits = 8,
                            StopBits = StopBits.One,
                            Parity = Parity.None,
                            ReadTimeout = 500,
                            NewLine = "\r"
                        };

                        _serial.Open();
                        _serial.ReadExisting(); //clear buffer

                        while (!_cancellationToken.IsCancellationRequested)
                        {
                            try
                            {
                                var number = _serial.ReadLine();
                                if (!string.IsNullOrWhiteSpace(number))
                                    Messenger.Default.Send(new CodeScannedMessage(number));
                            }
                            catch (TimeoutException)
                            {
                            }
                        }
                    }
                    catch (IOException e)
                    {
                        if(!silent)
                            DispatcherHelper.CheckBeginInvokeOnUI(() =>
                            {
                                MessageBox.Show(e.Message, "UWAGA", MessageBoxButton.OK, MessageBoxImage.Error);
                            });
                    }
                    catch (Exception e)
                    {
                        if (!silent)
                            DispatcherHelper.CheckBeginInvokeOnUI(() =>
                            {
                                MessageBox.Show(e.Message, "UWAGA", MessageBoxButton.OK, MessageBoxImage.Error);
                            });
                        Debug.WriteLine(e);
                    }
                    finally
                    {
                        _serial?.Close();
                    }
                }, _cancellationToken.Token);

                _scanTask.Start();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                Stop();
            }
        }

        public void Stop()
        {
            if (_scanTask != null)
            {
                _cancellationToken.Cancel();
                _scanTask?.Wait();
                _scanTask?.Dispose();
            }
            _cancellationToken?.Dispose();
            _cancellationToken = new CancellationTokenSource();
        }
    }

    public interface IScannerService
    {
        void Start(string port, bool silent = false);
        void Stop();
    }

    public class CodeScannedMessage : GenericMessage<string>
    {
        public CodeScannedMessage(string content) : base(content)
        {
        }

        public CodeScannedMessage(object sender, string content) : base(sender, content)
        {
        }

        public CodeScannedMessage(object sender, object target, string content) : base(sender, target, content)
        {
        }
    }
}