﻿using System;
using System.Text.RegularExpressions;
using DepotSystem.Database;

namespace DepotSystem.Extensions
{
    public static class BarcodeEncoderExtension
    {
        public static string ToBarcode(this Person person)
        {
            return $"PER:{person.Guid}";
        }

        public static string ToBarcode(this Product product)
        {
            return $"PRD:{product.Guid}";
        }

        public static string ToBarcode(this Product product, decimal quantity)
        {
            return $"OPR:{product.Guid}:{quantity}";
        }

        public static string EndBarcode()
        {
            return $"END:0";
        }
    }
}