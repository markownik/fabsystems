﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using DepotSystem.Database;
using DepotSystem.Models;
using DepotSystem.Services;
using FabSystems.Shared.Interfaces;
using FabSystems.Shared.Mvvm;
using FabSystems.Shared.Services;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Threading;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace DepotSystem.ViewModel
{
    public class CosmeticsUpdateViewModel : BaseVm
    {
        private readonly IProductsService _productsService;
        private readonly IScannerService _scannerService;
        private readonly ISettingsService _settingsService;
        private readonly IRepository<Person> _peopleRepository;

        private bool _isDeposit;

        public Person SelectedPerson { get; set; }
        public ObservableCollection<Person> People { get; set; } = new ObservableCollection<Person>();
        public ObservableCollection<ProductListModel> Products { get; set; } = new ObservableCollection<ProductListModel>();
        public string Title => _isDeposit ? "Dostawa (+)" : "Pobranie (-)";

        public ICommand ConfirmCommand => new RelayCommand(() => Confirm());
        public ICommand CancelCommand => new RelayCommand(Cancel);

        public CosmeticsUpdateViewModel(
            IProductsService productsService, 
            IScannerService scannerService, 
            ISettingsService settingsService, 
            IRepository<Person> peopleRepository)
        {
            _productsService = productsService;
            _scannerService = scannerService;
            _settingsService = settingsService;
            _peopleRepository = peopleRepository;
        }

        protected override void OnNavigatedTo(params object[] parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.Length > 0 && (parameters[0] is bool deposit))
                _isDeposit = deposit;

            People.Clear();
            foreach (var person in _peopleRepository.GetAll())
                People.Add(person);

            Products.Clear();
            
            MessengerInstance.Register<PersonalBarcodeMessage>(this, PersonScanned);
            MessengerInstance.Register<ProductBarcodeMessage>(this, ProductScanned);
            MessengerInstance.Register<ResetBarcodeMessage>(this, Confirm);
            MessengerInstance.Register<CodeScannedMessage>(this, BarcodeReceived);
            _scannerService.Start(_settingsService.ComPort);

            RaisePropertyChanged(nameof(People));
            RaisePropertyChanged(nameof(Title));
            RaisePropertyChanged(nameof(Products));
        }

        private void BarcodeReceived(CodeScannedMessage obj)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                _productsService.ParseBarcode(obj.Content);
            });
        }

        private void ProductScanned(ProductBarcodeMessage obj)
        {
            var product = obj.Content;
            if (Products.Any(p => p.Id == product.Id))
            {
                var count = Products.Single(p => p.Id == product.Id).Count;
                Products.Remove(Products.Single(p => p.Id == product.Id));
                RaisePropertyChanged(nameof(Products));
                Products.Add(new ProductListModel()
                {
                    Id = product.Id,
                    Name = product.Name,
                    Count = count + 1
                });
            }
            else
            {
                Products.Add(new ProductListModel()
                {
                    Id = product.Id,
                    Name = product.Name,
                    Count = 1
                });
            }

            RaisePropertyChanged(nameof(Products));
        }

        private void PersonScanned(PersonalBarcodeMessage obj)
        {
            SelectedPerson = People.Single(p => p.Id == obj.Content.Id);
            RaisePropertyChanged(nameof(SelectedPerson));
        }

        private void Confirm(ResetBarcodeMessage obj = null)
        {
            try
            {
                if(SelectedPerson == null)
                    throw new OperationException("Przed dodaniem pobrania/dostawy produktów musisz wybrać osobę!");

                var time = DateTime.Now;

                foreach (var product in Products)
                {
                    if (_isDeposit)
                        _productsService.Deposit(product.Id, product.Count, SelectedPerson, time);
                    else
                        _productsService.Withdraw(product.Id, -product.Count, SelectedPerson, time);
                }
                
            }
            catch (OperationException e)
            {
                MessageBox.Show(CurrentWindow, e.Message, "UWAGA", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception e)
            {
                MessageBox.Show(CurrentWindow, $"Wystąpił nieoczekiwany błąd: {e}", "UWAGA", MessageBoxButton.OK, MessageBoxImage.Error);
                Debug.WriteLine(e);
            }

            CurrentWindow?.Close();
        }

        private void Cancel()
        {
            CurrentWindow?.Close();
        }

        protected override void OnNavigatedFrom()
        {
            base.OnNavigatedFrom();
            MessengerInstance.Unregister(this);
            _scannerService.Stop();
        }
    }
}