﻿using System;
using System.Windows.Input;
using DepotSystem.Database;
using DepotSystem.Services;
using FabSystems.Shared.Mvvm;
using GalaSoft.MvvmLight.CommandWpf;

namespace DepotSystem.ViewModel
{
    public class ReportRangePickerViewModel : BaseVm
    {
        private readonly IReportGeneratorService _reportGeneratorService;
        private Category _category;
        private ReportType _type;

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public DateTime DateMax => DateTime.Today.AddDays(1).AddSeconds(-1);

        public ICommand GenerateReport => new RelayCommand(MakeReport);

        public ReportRangePickerViewModel(IReportGeneratorService reportGeneratorService)
        {
            _reportGeneratorService = reportGeneratorService;
        }
        
        protected override void OnNavigatedTo(params object[] parameters)
        {
            Clear();

            if (parameters.Length == 0 || !(parameters[0] is Category category))
                return;
            
            _category = category;

            if (parameters.Length > 1)
            {
                if (parameters[1] is ReportType type)
                    _type = type;
            }
        }

        private void Clear()
        {
            _type = ReportType.Warehouse;
            _category = Category.Cosmetics;
            DateFrom = DateTime.Today;
            DateTo = DateTime.Today.AddDays(1).AddSeconds(-1);

            RaisePropertyChanged(nameof(DateFrom));
            RaisePropertyChanged(nameof(DateTo));
            RaisePropertyChanged(nameof(DateMax));
        }

        private void MakeReport()
        {
            switch (_type)
            {
                default:
                case ReportType.Warehouse:
                    _reportGeneratorService.GenerateReport(_category, DateFrom, DateTo);
                    break;
                case ReportType.Sellers:
                    _reportGeneratorService.GenerateSellersReport(_category, DateFrom, DateTo);
                    break;
            }
            CurrentWindow?.Close();
        }
    }
}