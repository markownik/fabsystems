﻿using System.Windows;
using System.Windows.Input;
using DepotSystem.Database;
using FabSystems.Shared.Interfaces;
using FabSystems.Shared.Mvvm;
using GalaSoft.MvvmLight.Command;

namespace DepotSystem.ViewModel
{
    public class UpdateProductViewModel : BaseVm, IProduct, IModel<Product>
    {
        private readonly IRepository<Product> _productsRepository;
        
        private bool _changed;

        private Product _model = null;

        public string Title => _model != null ? "Edytuj produkt" : "Dodaj nowy produkt";
        public ICommand SaveCommand => new RelayCommand(Save);
        public ICommand CancelCommand => new RelayCommand(Cancel);

        public string Name { get; set; }
        public Precision Type { get; set; }
        public Category Category { get; set; }
        public string Guid { get; set; }
        public bool Active { get; set; }

        public UpdateProductViewModel(IRepository<Product> productsRepository)
        {
            _productsRepository = productsRepository;
        }

        protected override void OnNavigatedTo(params object[] parameters)
        {
            if (parameters.Length == 0 || !(parameters[0] is Product updated))
            {
                Clear();
                return;
            }
            _model = updated;
            RaisePropertyChanged(() => Title);
            LoadModel(_model);
            _changed = false;
        }

        private void Save()
        {
            _model ??= _productsRepository.Create();
            _model.Load(this);
            _productsRepository.Update(_model);

            Clear();
            CurrentWindow?.Close();
        }

        private void Cancel()
        {
            if (_changed)
                if (Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, "Czy na pewno chcesz anulować wprowadzone zmiany?", "UWAGA", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) != MessageBoxResult.Yes)
                    return;

            Clear();
            CurrentWindow?.Close();
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);
            _changed = true;
        }

        public void LoadModel(Product input)
        {
            Name = input.Name;
            Category = input.Category;
            Type = input.Type;
            Guid = input.Guid;
            Active = input.Active;

            RaisePropertyChanged(() => Name);
            RaisePropertyChanged(() => Category);
            RaisePropertyChanged(() => Type);
        }

        public void Clear()
        {
            _model = null;
            Name = string.Empty;
            Category = Category.Warehouse;
            Type = Precision.Full;
            Guid = string.Empty;
            Active = true;

            RaisePropertyChanged(() => Name);
            RaisePropertyChanged(() => Category);
            RaisePropertyChanged(() => Type);
        }
    }

}