﻿using System.Windows;
using System.Windows.Input;
using DepotSystem.Database;
using FabSystems.Shared.Interfaces;
using FabSystems.Shared.Mvvm;
using GalaSoft.MvvmLight.Command;

namespace DepotSystem.ViewModel
{
    public class UpdatePersonViewModel : BaseVm, IPerson, IModel<Person>
    {
        private readonly IRepository<Person> _peopleRepository;

        private bool _changed;

        private Person _model = null;

        public string Title => _model != null ? "Edytuj osobę" : "Dodaj nową osobę";
        public ICommand SaveCommand => new RelayCommand(Save);
        public ICommand CancelCommand => new RelayCommand(Cancel);

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Guid { get; set; }
        public bool Active { get; set; }

        public UpdatePersonViewModel(IRepository<Person> peopleRepository)
        {
            _peopleRepository = peopleRepository;
        }

        protected override void OnNavigatedTo(params object[] parameters)
        {
            if (parameters.Length == 0 || !(parameters[0] is Person updated))
            {
                Clear();
                return;
            }
            _model = updated;
            RaisePropertyChanged(() => Title);
            LoadModel(_model);
            _changed = false;
        }

        private void Save()
        {
            _model ??= _peopleRepository.Create();
            _model.Load(this);
            _peopleRepository.Update(_model);

            Clear();
            CurrentWindow?.Close();
        }

        private void Cancel()
        {
            if (_changed)
                if (Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, "Czy na pewno chcesz anulować wprowadzone zmiany?", "UWAGA", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) != MessageBoxResult.Yes)
                    return;

            Clear();
            CurrentWindow?.Close();
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);
            _changed = true;
        }

        public void LoadModel(Person input)
        {
            FirstName = input.FirstName;
            LastName = input.LastName;
            Guid = input.Guid;
            Active = input.Active;

            RaisePropertyChanged(() => FirstName);
            RaisePropertyChanged(() => LastName);
        }

        public void Clear()
        {
            _model = null;
            FirstName = string.Empty;
            LastName = string.Empty;
            Guid = string.Empty;
            Active = true;
        }
    }

}