﻿using CommonServiceLocator;
using DepotSystem.Database;
using DepotSystem.Services;
using DepotSystem.Windows;
using FabSystems.Shared.Interfaces;
using FabSystems.Shared.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;

namespace DepotSystem.ViewModel
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (!ViewModelBase.IsInDesignModeStatic)
            {
                SimpleIoc.Default.Register<ISettingsService>(() => new SettingsService(), true);
                SimpleIoc.Default.Register<IChecksumService>(() => new ChecksumService());
                SimpleIoc.Default.Register<ILabelPrinterService>(() => new LabelPrinterService());
                SimpleIoc.Default.Register<IScannerService>(() => new ScannerService());
                SimpleIoc.Default.Register<IRepository<Person>>(() => new PeopleRepository());
                SimpleIoc.Default.Register<IRepository<Product>>(() => new ProductsRepository());
                SimpleIoc.Default.Register<IProductsService>(
                    () => new ProductsService(
                    SimpleIoc.Default.GetInstance<IRepository<Product>>(), 
                    SimpleIoc.Default.GetInstance<IRepository<Person>>()));
                SimpleIoc.Default.Register<IReportGeneratorService>(() => new ReportGeneratorService());
                SimpleIoc.Default.Register<IOperationsService>(() => new OperationsService());
            }

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<ReportRangePickerViewModel>();
            SimpleIoc.Default.Register<PeopleManagerViewModel>();
            SimpleIoc.Default.Register<UpdatePersonViewModel>();
            SimpleIoc.Default.Register<ProductsManagerViewModel>();
            SimpleIoc.Default.Register<UpdateProductViewModel>();
            SimpleIoc.Default.Register<CosmeticsUpdateViewModel>();
        }

        public MainViewModel Main => ServiceLocator.Current.GetInstance<MainViewModel>();

        public ReportRangePickerViewModel ReportRangePicker =>
            ServiceLocator.Current.GetInstance<ReportRangePickerViewModel>();

        public PeopleManagerViewModel PeopleManager => ServiceLocator.Current.GetInstance<PeopleManagerViewModel>();

        public ProductsManagerViewModel ProductsManager => ServiceLocator.Current.GetInstance<ProductsManagerViewModel>();

        public UpdatePersonViewModel UpdatePerson => ServiceLocator.Current.GetInstance<UpdatePersonViewModel>();

        public UpdateProductViewModel UpdateProduct => ServiceLocator.Current.GetInstance<UpdateProductViewModel>();

        public CosmeticsUpdateViewModel UpdateCosmetics => ServiceLocator.Current.GetInstance<CosmeticsUpdateViewModel>();
    }
}