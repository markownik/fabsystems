﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO.Ports;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using DepotSystem.Database;
using DepotSystem.Extensions;
using DepotSystem.Models;
using DepotSystem.Services;
using DepotSystem.Windows;
using FabSystems.Shared.Extensions;
using FabSystems.Shared.Mvvm;
using FabSystems.Shared.Services;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Threading;

namespace DepotSystem.ViewModel
{
    public class MainViewModel : BaseVm
    {
        private readonly ISettingsService _settingsService;
        private readonly ILabelPrinterService _labelPrinterService;
        private readonly IProductsService _productsService;
        private readonly IScannerService _scannerService;
        private readonly IOperationsService _operationsService;
        public ICommand RefreshPortCommand => new RelayCommand(RefreshComPorts);

        public ICommand RefreshPrinterCommand => new RelayCommand(RefreshPrinters);

        public ICommand SaveCommand => new RelayCommand(SaveSettings);

        public ICommand ShowReportWindowCommand => new RelayCommand(() => { ShowReport(); });

        public ICommand ShowSellerReportWindowCommand => new RelayCommand(() => { ShowReport(ReportType.Sellers); });

        public ICommand EditPeople => new RelayCommand(OpenPeopleManager);

        public ICommand EditProducts => new RelayCommand(OpenProductsManager);

        public ICommand WithdrawCosmeticsCommand => new RelayCommand(() =>
        {
            OpenDialog<UpdateCosmeticsWindow>(false);
            ReloadProucts();
        });

        public ICommand DepositCosmeticsCommand => new RelayCommand(() =>
        {
            OpenDialog<UpdateCosmeticsWindow>(true);
            ReloadProucts();
        });

        public ICommand PrintBarcodeCommand => new RelayCommand(PrintBarcode);

        public ObservableCollection<ProductListModel> DepotProducts { get; set; } = new ObservableCollection<ProductListModel>();

        public ObservableCollection<ProductListModel> CosmeticProducts { get; set; } = new ObservableCollection<ProductListModel>();

        public ObservableCollection<ProductListModel> CosmeticProducts2 { get; set; } = new ObservableCollection<ProductListModel>();

        public ObservableCollection<LogListModel> LogsCollection { get; set; } = new ObservableCollection<LogListModel>();

        public ObservableCollection<string> ComPorts { get; set; } = new ObservableCollection<string>();

        public ObservableCollection<string> Printers { get; set; } = new ObservableCollection<string>();

        public ProductListModel SeletedCosmetic { get; set; }

        public TabItem SelectedTab { get; set; }

        public string Printer { get; set; }

        public string ComPort { get; set; }

        public int PrintLabelCount { get; set; } = 1;

        private bool HeadlessOperation => SelectedTab.Name.Equals("Warehouse");
        private Person SelectedPerson = null;

        public string SelectedPersonString => SelectedPerson != null ? $"{SelectedPerson.FirstName} {SelectedPerson.LastName}" : "brak";

        public MainViewModel(
            ISettingsService settingsService, 
            ILabelPrinterService labelPrinterService, 
            IProductsService productsService, 
            IScannerService scannerService,
            IOperationsService operationsService)
        {
            _settingsService = settingsService;
            _labelPrinterService = labelPrinterService;
            _productsService = productsService;
            _scannerService = scannerService;
            _operationsService = operationsService;

            ComPort = _settingsService.ComPort;
            Printer = _settingsService.Printer;

            RefreshPrinters();
            RefreshComPorts();
            ReloadProucts();
            ReloadLogs();
            MessengerInstance.Register<CodeScannedMessage>(this, BarcodeReceived);
            MessengerInstance.Register<PersonalBarcodeMessage>(this, PersonalBarcodeReceived);
            MessengerInstance.Register<ProductBarcodeMessage>(this, ProductBarcodeReceived);
            MessengerInstance.Register<OperationBarcodeMessage>(this, OperationBarcodeReceived);
            MessengerInstance.Register<ResetBarcodeMessage>(this, ResetHeadless);
        }

        private void ReloadProucts(Category? category = null)
        {
            if (category == Category.Warehouse || category is null)
            {
                DepotProducts.Clear();
                foreach (var product in _productsService.GetCountedProducts(Category.Warehouse))
                    DepotProducts.Add(product);
            }
            if (category == Category.Cosmetics || category is null)
            {
                CosmeticProducts.Clear();
                foreach (var product in _productsService.GetCountedProducts(Category.Cosmetics))
                    CosmeticProducts.Add(product);
            }
            if (category == Category.Cosmetics2 || category is null)
            {
                CosmeticProducts2.Clear();
                foreach (var product in _productsService.GetCountedProducts(Category.Cosmetics2))
                    CosmeticProducts2.Add(product);
            }
        }

        private void RefreshComPorts()
        {
            var temp = ComPort;
            ComPorts.Clear();

            foreach (string port in SerialPort.GetPortNames())
                ComPorts.Add(port);


            if (!string.IsNullOrEmpty(temp) && !ComPorts.Contains(temp))
                ComPorts.Add(temp);
            ComPort = temp;

            RaisePropertyChanged(() => ComPorts);
            RaisePropertyChanged(() => ComPort);
        }

        private void RefreshPrinters()
        {
            var temp = Printer;
            Printers.Clear();

            foreach (string printer in _labelPrinterService.Printers)
                Printers.Add(printer);

            if (!string.IsNullOrEmpty(temp) && !Printers.Contains(temp))
                Printers.Add(temp);

            Printer = temp;

            _labelPrinterService.SelectedPrinter = Printer;
            RaisePropertyChanged(() => Printers);
            RaisePropertyChanged(() => Printer);
        }

        private void SaveSettings()
        {
            _labelPrinterService.SelectedPrinter = Printer;
            _settingsService.Printer = Printer;
            _settingsService.ComPort = ComPort;
        }

        private void ShowReport(ReportType type = ReportType.Warehouse)
        {
            var category = EnumHelper.GetValueFromDescription<Category>(SelectedTab?.Header as string);
            OpenDialog<ReportRangePickerWindow>(category, type);
        }

        private void OpenPeopleManager()
        {
            OpenWindow<PeopleManagerWindow>();
            ReloadProucts();
        }

        private void OpenProductsManager()
        {
            OpenWindow<ProductsManagerWindow>();
            ReloadProucts();
        }

        private void ReloadLogs()
        {
            LogsCollection.Clear();
            foreach (var log in _operationsService.GetLogs(200))
                LogsCollection.Add(log);
        }

        private void BarcodeReceived(CodeScannedMessage obj)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                if (HeadlessOperation)
                {
                    _productsService.ParseBarcode(obj.Content);
                }
            });
        }

        private void OperationBarcodeReceived(OperationBarcodeMessage obj)
        {
            try
            {
                if (HeadlessOperation)
                {
                    if (SelectedPerson != null)
                    {
                        if (obj.Content.Value > 0)
                            _productsService.Deposit(obj.Content.Key, obj.Content.Value, SelectedPerson);
                        else
                            _productsService.Withdraw(obj.Content.Key, obj.Content.Value, SelectedPerson);
                    }
                    else
                        throw new OperationException(
                            "Nie można wykonać operacji bez wybranej osoby! (Najpierw zeskanuj kod osoby pobierającej)");

                    ReloadProucts();
                }
            }
            catch (OperationException e)
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, e.Message, "UWAGA", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
            catch (Exception e)
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, $"Wystąpił nieoczekiwany błąd: {e}", "UWAGA", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                Debug.WriteLine(e);
            }
        }

        private void ProductBarcodeReceived(ProductBarcodeMessage obj)
        {
            //if (SelectedTab.Name.Equals("Warehouse") || SelectedTab.Name.Equals("Cosmetics"))
            //    Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, $"Produkt: \"{obj.Content.Name}\", pozostało {_productsService.GetProductCount(obj.Content)} szt.", "INFO",
            //        MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void PersonalBarcodeReceived(PersonalBarcodeMessage obj)
        {
            if (HeadlessOperation)
                SelectedPerson = obj.Content;
            RaisePropertyChanged(nameof(SelectedPersonString));
        }

        private void ResetHeadless(ResetBarcodeMessage obj = null)
        {
            SelectedPerson = null;
            RaisePropertyChanged(nameof(SelectedPersonString));
        }

        private void PrintBarcode()
        {
            if(SeletedCosmetic != null)
                _labelPrinterService.PrintProductBarcode(_productsService.GetProduct(SeletedCosmetic?.Guid).ToBarcode(), SeletedCosmetic?.Name, Encoding.UTF8.GetString(Resources.barcode), PrintLabelCount);
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == nameof(SelectedTab))
            {
                if (HeadlessOperation)
                {
                    _scannerService.Start(_settingsService.ComPort, true);
                }
                else
                {
                    new Task(() =>
                    {
                        _scannerService.Stop();
                    }).Start();
                    ResetHeadless();
                }

                try
                {
                    var category = EnumHelper.GetValueFromDescription<Category>(SelectedTab?.Header as string);
                    ReloadProucts(category);
                }
                catch { }

                if (SelectedTab.Name.Equals("Logs"))
                    ReloadLogs();

                SeletedCosmetic = null;
            }
        }
    }
}