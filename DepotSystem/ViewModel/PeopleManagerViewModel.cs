﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using DepotSystem.Database;
using DepotSystem.Services;
using DepotSystem.Windows;
using FabSystems.Shared.Interfaces;
using FabSystems.Shared.Mvvm;
using GalaSoft.MvvmLight.Command;

namespace DepotSystem.ViewModel
{
    public class PeopleManagerViewModel : BaseVm
    {
        private readonly IRepository<Person> _peopleRepository;
        private readonly IReportGeneratorService _reportGeneratorService;

        public ObservableCollection<Person> People { get; set; } = new ObservableCollection<Person>();

        public Person SelectedPerson { get; set; }

        public ICommand AddPerson => new RelayCommand(Add);

        public ICommand EditPerson => new RelayCommand(Edit, () => SelectedPerson != null);

        public ICommand DeletePerson => new RelayCommand(Delete, () => SelectedPerson != null);

        public ICommand GenerateBookletCommand => new RelayCommand(GenerateBooklet);

        
        public PeopleManagerViewModel(IRepository<Person> peopleRepository, IReportGeneratorService reportGeneratorService)
        {
            _peopleRepository = peopleRepository;
            _reportGeneratorService = reportGeneratorService;
            ReloadData();
        }

        private void ReloadData()
        {
            People.Clear();
            foreach (var variant in _peopleRepository.GetAll())
                People.Add(variant);
        }

        private void Add()
        {
            OpenDialog<UpdatePersonWindow>();
            ReloadData();
        }

        private void Edit()
        {
            if (SelectedPerson == null)
                return;

            OpenDialog<UpdatePersonWindow>(SelectedPerson);
            ReloadData();
        }

        private void Delete()
        {
            if (SelectedPerson == null)
                return;

            if (Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, "Czy na pewno chcesz usunąć wybraną osobę?", "UWAGA", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
            {
                _peopleRepository.Delete(SelectedPerson);
                SelectedPerson = null;
                ReloadData();
            }
        }

        private void GenerateBooklet()
        {
            _reportGeneratorService.GeneratePeopleBooklet();
        }
    }
}