﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using DepotSystem.Database;
using DepotSystem.Services;
using DepotSystem.Windows;
using FabSystems.Shared.Interfaces;
using FabSystems.Shared.Mvvm;
using GalaSoft.MvvmLight.Command;

namespace DepotSystem.ViewModel
{
    public class ProductsManagerViewModel : BaseVm
    {
        private readonly IRepository<Product> _productsRepository;
        private readonly IReportGeneratorService _reportGeneratorService;

        public ObservableCollection<Product> Products { get; set; } = new ObservableCollection<Product>();

        public Product SelectedProduct { get; set; }

        public ICommand AddProduct => new RelayCommand(Add);

        public ICommand EditProduct => new RelayCommand(Edit, () => SelectedProduct != null);

        public ICommand DeleteProduct => new RelayCommand(Delete, () => SelectedProduct != null);

        public ICommand GenerateBookletCommand => new RelayCommand(GenerateBooklet);

        public ProductsManagerViewModel(IRepository<Product> productsRepository, IReportGeneratorService reportGeneratorService)
        {
            _productsRepository = productsRepository;
            _reportGeneratorService = reportGeneratorService;
            ReloadData();
        }

        private void ReloadData()
        {
            Products.Clear();
            foreach (var variant in _productsRepository.GetAll())
                Products.Add(variant);
        }

        private void Add()
        {
            OpenDialog<UpdateProductWindow>();
            ReloadData();
        }

        private void Edit()
        {
            if (SelectedProduct == null)
                return;

            OpenDialog<UpdateProductWindow>(SelectedProduct);
            ReloadData();
        }

        private void Delete()
        {
            if (SelectedProduct == null)
                return;

            if (Xceed.Wpf.Toolkit.MessageBox.Show(CurrentWindow, "Czy na pewno chcesz usunąć wybrany produkt?", "UWAGA", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
            {
                _productsRepository.Delete(SelectedProduct);
                SelectedProduct = null;
                ReloadData();
            }
        }

        private void GenerateBooklet()
        {
            _reportGeneratorService.GenerateProductsBooklet();
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == nameof(SelectedProduct))
            {
                RaisePropertyChanged(nameof(EditProduct));
                RaisePropertyChanged(nameof(DeleteProduct));
            }
        }
    }
}