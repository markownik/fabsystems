﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using DepotSystem.Database;
using DepotSystem.Extensions;
using Microsoft.Win32;
using Syncfusion.HtmlConverter;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using ZXing;
using ZXing.QrCode;
using Size = System.Drawing.Size;

namespace DepotSystem.Services
{
    public class ReportGeneratorService : IReportGeneratorService
    {
        private string TempFileName =>
            $"{Path.Combine(Path.GetTempPath(), $"Raport-{DateTime.Now:yyyyMMdd-HHmmss}.pdf")}";

        private HtmlToPdfConverter _htmlConverter;

        public ReportGeneratorService()
        {
            //_htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.Blink)
            //{
            //    ConverterSettings = new BlinkConverterSettings
            //    {
            //        BlinkPath = @"./BlinkBinaries/",
            //        EnableJavaScript = true,
            //        ViewPortSize = new Size(1080, 0),
            //        Margin = new PdfMargins() { Top = 20f, Bottom = 20f },
            //        TempPath = Path.GetTempPath(),
            //        TempFileName = Guid.NewGuid().ToString()
            //    }
            //};

            _htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit)
            {
                ConverterSettings = new WebKitConverterSettings()
                {
                    WebKitPath = @"./QtBinaries/",
                    EnableJavaScript = true,
                    WebKitViewPort = new Size(1080, 0),
                    Margin = new PdfMargins() { Top = 20f, Bottom = 20f },
                    TempPath = Path.GetTempPath(),
                    TempFileName = Guid.NewGuid().ToString()
                }
            };
        }

        public void GenerateReport(Category category, DateTime from, DateTime to)
        {
            try
            {
                var txt = GenerateWarehouseReport(from, to, category);
                var path = GeneratePdf(txt, TempFileName);
                OpenWithDefaultProgram(path);
            }
            catch (Exception e)
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(e.Message, "BŁĄD",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        public void GenerateSellersReport(Category category, DateTime from, DateTime to)
        {
            try
            {
                var txt = GenerateSellersReport(from, to, category);
                var path = GeneratePdf(txt, TempFileName);
                OpenWithDefaultProgram(path);
            }
            catch (Exception e)
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(e.Message, "BŁĄD",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        public void GeneratePeopleBooklet()
        {
            using var db = new DepotSystemDbContext();
            var people = db.People.Where(p => p.Active).OrderBy(p => p.LastName).ThenBy(p => p.FirstName).ToList();
            people.Add(new Person()
            {
                FirstName = "ZATWIERDŹ",
                Guid = "0"
            });

            var tableInsert = string.Empty;
            var count = people.Count;
            while (count % 4 != 0) count++;

            for (int i = 0; i < count; i++)
            {
                if (i == 0)
                    tableInsert = $"{tableInsert}<tr>";
                if (i % 4 == 0)
                    tableInsert = $"{tableInsert}</tr><tr>";

                tableInsert = $"{tableInsert}<td>";
                if (i < people.Count)
                    if (people[i].Guid == "0")
                        tableInsert = $"{tableInsert}<img class=\"qr\" src=\"{GetBase64QrCode(BarcodeEncoderExtension.EndBarcode())}\"/><p>{people[i]}</p>";
                    else
                        tableInsert = $"{tableInsert}<img class=\"qr\" src=\"{GetBase64QrCode(people[i].ToBarcode())}\"/><p>{people[i]}</p>";
                tableInsert = $"{tableInsert}</td>";
            }
            tableInsert = $"{tableInsert}</tr>";

            var txt = Resources.booklet_qr
                .Replace("<title/>", "LISTA OSÓB")
                .Replace("<table_body/>", tableInsert);

            try
            {
                var path = GeneratePdf(txt, TempFileName);
                OpenWithDefaultProgram(path);
            }
            catch (Exception e)
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(e.Message, "BŁĄD",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        public void GenerateProductsBooklet(Category category = Category.Warehouse)
        {
            using var db = new DepotSystemDbContext();
            var products = db.Products.Where(p => p.Active && p.Category == category).ToList().OrderBy(p => p.Name);

            var tableInsert = string.Empty;

            foreach (var product in products)
            {
                if (product.Type == Precision.Half)
                {
                    var temp = Resources.table_row_qr
                        .Replace("<1>",
                            $"<img class=\"qr\" src=\"{GetBase64QrCode(product.ToBarcode(-1))}\"/><p>{product.Name} (-1)</p>")
                        .Replace("<2>",
                            $"<img class=\"qr\" src=\"{GetBase64QrCode(product.ToBarcode((decimal)-0.5))}\"/><p>{product.Name} (-0.5)</p>")
                        .Replace("<3>",
                            $"<img class=\"qr\" src=\"{GetBase64QrCode(product.ToBarcode((decimal)0.5))}\"/><p>{product.Name} (+0.5)</p>")
                        .Replace("<4>",
                            $"<img class=\"qr\" src=\"{GetBase64QrCode(product.ToBarcode(1))}\"/><p>{product.Name} (+1)</p>");
                    tableInsert = $"{tableInsert}{temp}";
                }
                else
                {
                    var temp = Resources.table_row_qr
                        .Replace("<1>",
                            $"<img class=\"qr\" src=\"{GetBase64QrCode(product.ToBarcode(-1))}\"/><p>{product.Name} (-1)</p>")
                        .Replace("<2>", "")
                        .Replace("<3>", "")
                        .Replace("<4>",
                            $"<img class=\"qr\" src=\"{GetBase64QrCode(product.ToBarcode(1))}\"/><p>{product.Name} (+1)</p>");
                    tableInsert = $"{tableInsert}{temp}";
                }
            }

            var txt = Resources.booklet_qr
                .Replace("<title/>", "LISTA PRODUKTÓW")
                .Replace("<table_body/>", tableInsert);

            try
            {
                var path = GeneratePdf(txt, TempFileName);
                OpenWithDefaultProgram(path);
            }
            catch (Exception e)
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(e.Message, "BŁĄD",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private string GetBase64QrCode(string input)
        {
            var qr = new BarcodeWriter()
            {
                Options = new QrCodeEncodingOptions
                {
                    DisableECI = true,
                    CharacterSet = "UTF-8",
                    Width = 250,
                    Height = 250,
                    Margin = 0
                },
                Format = BarcodeFormat.QR_CODE
            };
            var bitmap = new Bitmap(qr.Write(input));
            using var ms = new MemoryStream();
            bitmap.Save(ms, ImageFormat.Png);
            return $"data:image/png;base64,{Convert.ToBase64String(ms.ToArray())}";
        }

        private string GenerateWarehouseReport(DateTime from, DateTime to, Category category = Category.Warehouse)
        {
            var productList = new List<ReportProductUsage>();
            using var db = new DepotSystemDbContext();
            var products = db.Products.Where(p => p.Active && p.Category == category).Include(p => p.Operations).ToList();

            foreach (var product in products)
            {
                var uid = db.People
                    .SingleOrDefault(p => p.FirstName.Contains("Dostawa") || p.LastName.Contains("Dostawa"))?.Id;

                var beggining = product.Operations.Where(o => o.Date <= from).Sum(o => o.Quantity);
                var diff = product.Operations.Where(o => o.Date > from && o.Date <= to).Sum(o => o.Quantity);
                var usage = -product.Operations.Where(o => o.Date > from && o.Date <= to && o.PersonId != uid).Sum(o => o.Quantity);

                if (usage < 0)
                    usage = 0;

                productList.Add(new ReportProductUsage()
                {
                    Name = product.Name,
                    BeginCount = beggining,
                    TotalCount = beggining + diff,
                    Usage = usage 
                });
            }

            productList = productList.OrderBy(p => p.Name).ToList();

            var tableInsert = string.Empty;
            foreach (var product in productList)
            {
                tableInsert =
                    $"{tableInsert}{Resources.table_row_1.Replace("<name>", product.Name).Replace("<beginning>", product.BeginCount.ToString()).Replace("<sumup>", product.TotalCount.ToString()).Replace("<difference>", product.Usage.ToString())}";
            }

            var txt = Resources.raport_standard
                .Replace("<time_from>", $"{from:yyyy-MM-dd HH:mm:ss}")
                .Replace("<time_to>", $"{to:yyyy-MM-dd HH:mm:ss}")
                .Replace("<table_body>", $"{tableInsert}");
            return txt;
        }

        private string GenerateSellersReport(DateTime from, DateTime to, Category category = Category.Cosmetics)
        {
            var productList = new List<ReportProductUsage>();
            var personList = new List<ReportPersonValue>();

            using var db = new DepotSystemDbContext();
            var products = db.Products.Where(p => p.Active && p.Category == category).Include(p => p.Operations).ToList();
            foreach (var product in products)
            {
                productList.Add(new ReportProductUsage()
                {
                    Name = product.Name,
                    TotalCount = Math.Abs(product.Operations.Where(o => o.Date > from && o.Date <= to).Sum(o => o.Quantity)),
                });
            }

            var people = db.People.Where(p => p.Active).ToList();
            var operations = db.Operations
                .Where(o => o.Date > from && o.Date <= to && o.Product.Category == category)
                .Include(o => o.Product).ToList();
            foreach (var person in people)
            {
                personList.Add(new ReportPersonValue()
                {
                    Name = person.ToString(),
                    TotalCount = Math.Abs(operations.Where(o => o.PersonId == person.Id).Sum(o => o.Quantity)),
                });
            }

            productList = productList.OrderByDescending(p => p.TotalCount).ToList();
            personList = personList.OrderByDescending(p => p.TotalCount).ToList();

            var tableInsert1 = string.Empty;
            foreach (var product in productList)
            {
                tableInsert1 =
                    $"{tableInsert1}{Resources.table_row_2.Replace("<col_1>", product.Name).Replace("<col_2>", product.TotalCount.ToString())}";
            }

            var tableInsert2 = string.Empty;
            foreach (var person in personList)
            {
                tableInsert2 =
                    $"{tableInsert2}{Resources.table_row_2.Replace("<col_1>", person.Name).Replace("<col_2>", person.TotalCount.ToString())}";
            }

            var txt = Resources.raport_kosmetyki
                .Replace("<time_from>", $"{from:yyyy-MM-dd HH:mm:ss}")
                .Replace("<time_to>", $"{to:yyyy-MM-dd HH:mm:ss}")
                .Replace("<table_body_1>", tableInsert1)
                .Replace("<table_body_2>", tableInsert2);
            return txt;
        }

        private string GeneratePdf(string html, string path)
        {
            PdfDocument document = _htmlConverter.Convert(html, Path.GetPathRoot(path));
            document.Save(path);
            document.DisposeOnClose(document);
            document.Close(true);
            return path;
        }

        private void OpenWithDefaultProgram(string path)
        {
            Process.Start(new ProcessStartInfo("explorer", path));
        }

        private class ReportProductUsage
        {
            public string Name { get; set; }
            public decimal BeginCount { get; set; }
            public decimal TotalCount { get; set; }
            public decimal Usage { get; set; }
        }

        private class ReportPersonValue
        {
            public string Name { get; set; }
            public decimal TotalCount { get; set; }
        }
    }

    public enum ReportType
    {
        [Description("Magazynowy")]
        Warehouse,

        [Description("Sprzedawcy")]
        Sellers
    }

    public interface IReportGeneratorService
    {
        void GenerateReport(Category category, DateTime from, DateTime to);

        void GenerateSellersReport(Category category, DateTime from, DateTime to);

        void GeneratePeopleBooklet();

        void GenerateProductsBooklet(Category category = Category.Warehouse);
    }
}