﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using DepotSystem.Database;
using FabSystems.Shared.Interfaces;

namespace DepotSystem.Services
{
    public class PeopleRepository : IRepository<Person>
    {
        public IEnumerable<Person> GetAll()
        {
            using var db = new DepotSystemDbContext();
            IEnumerable<Person> result = db.People.Where(p => p.Active).OrderBy(p => p.LastName).ThenBy(p => p.FirstName).ToList();
            return result;
        }

        public Person GetByGuid(string guid)
        {
            using var db = new DepotSystemDbContext();
            var result = db.People.SingleOrDefault(p => p.Guid == guid);
            return result;
        }

        public Person GetById(int id)
        {
            using var db = new DepotSystemDbContext();
            var result = db.People.SingleOrDefault(p => p.Id == id);
            return result;
        }

        public void Update(Person element)
        {
            using var db = new DepotSystemDbContext();
            db.People.AddOrUpdate(element);
            db.SaveChanges();
        }

        public bool Delete(Person element)
        {
            element.Active = false;
            using var db = new DepotSystemDbContext();
            db.People.AddOrUpdate(element);
            db.SaveChanges();
            return true;
        }

        public Person Create()
        {
            using var db = new DepotSystemDbContext();
            var result = db.People.Create();
            result.Active = true;
            result.Guid = Guid.NewGuid().ToString().Replace("-", "");
            db.SaveChanges();
            return result;
        }
    }
}