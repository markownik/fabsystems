﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Diagnostics.PerformanceData;
using System.Linq;
using System.Windows.Media.Converters;
using DepotSystem.Database;
using DepotSystem.Models;
using FabSystems.Shared.Interfaces;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using Xceed.Wpf.Toolkit.Themes;

namespace DepotSystem.Services
{
    public class ProductsService : IProductsService
    {
        private readonly PeopleRepository _peopleRepository;
        private readonly ProductsRepository _productRepository;

        public ProductsService(IRepository<Product> productRepository, IRepository<Person> peopleRepository)
        {
            _peopleRepository = peopleRepository as PeopleRepository;
            _productRepository = productRepository as ProductsRepository;
        }

        public List<ProductListModel> GetCountedProducts(Category category)
        {
            List<ProductListModel> result = new List<ProductListModel>();

            try
            {
                using var db = new DepotSystemDbContext();

                var products = db.Products.Where(p => p.Category == category && p.Active).OrderBy(p => p.Name).Include(p => p.Operations);
                foreach (var product in products)
                {
                    result.Add(new ProductListModel()
                    {
                        Id = product.Id,
                        Name = product.Name,
                        Count = product.Operations.Sum(o => o.Quantity),
                        Guid = product.Guid
                    });
                }

                return result.OrderBy(p => p.Name).ToList();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public Product GetProduct(string guid)
        {
            return _productRepository.GetByGuid(guid);
        }

        public decimal GetProductCount(Product product)
        {
            using var db = new DepotSystemDbContext();
            return db.Products.Where(p => p.Id == product.Id).Include(p => p.Operations).Single().Operations.Sum(o => o.Quantity);
        }

        public void Deposit(int productId, decimal quantity, Person person, DateTime? time = null)
        {
            time ??= DateTime.Now;

            var product = _productRepository.GetById(productId);

            Deposit(product, quantity, person, time);
        }

        public void Deposit(Product product, decimal quantity, Person person, DateTime? time = null)
        {
            time ??= DateTime.Now;

            using var db = new DepotSystemDbContext();
            var op = db.Operations.Create();
            op.Quantity = quantity;
            op.Date = (DateTime)time;
            op.ProductId = product.Id;
            op.PersonId = person.Id;
            db.Operations.AddOrUpdate(op);
            db.SaveChanges();
        }

        public void Withdraw(int productId, decimal quantity, Person person, DateTime? time = null)
        {
            time ??= DateTime.Now;

            var product = _productRepository.GetById(productId);
            if(Math.Abs(quantity) > GetProductCount(product))
                throw new OperationException($"Nie ma wystarczającej ilości produktu \"{product.Name}\" w magazynie do wykonania pobrania ({quantity} szt.)!");

            Withdraw(product, quantity, person, time);
        }

        public void Withdraw(Product product, decimal quantity, Person person, DateTime? time = null)
        {
            time ??= DateTime.Now;

            if (GetProductCount(product) > 0)
            {
                using var db = new DepotSystemDbContext();
                var op = db.Operations.Create();
                op.Quantity = quantity;
                op.Date = (DateTime)time;
                op.ProductId = product.Id;
                op.PersonId = person.Id;
                db.Operations.AddOrUpdate(op);
                db.SaveChanges();
            }
            else
                throw new OperationException($"Produktu ({product.Name}) nie ma w magazynie!");
        }

        public Type GetBarcodeType(string barcode)
        {
            switch (barcode.Split(':')[0])
            {
                default:
                    return null;
                case "PRD":
                    return typeof(Product);
                case "PER":
                    return typeof(Person);
                case "OPR":
                    return typeof(Operation);
            }
        }

        public void ParseBarcode(string barcode)
        {
            var type = GetBarcodeType(barcode);
            if(type == null)
                Messenger.Default.Send(new ResetBarcodeMessage());
            else if (type == typeof(Person))
                Messenger.Default.Send(new PersonalBarcodeMessage(_peopleRepository.GetByGuid(barcode.Split(':')[1])));
            else if (type == typeof(Product))
                Messenger.Default.Send(new ProductBarcodeMessage(_productRepository.GetByGuid(barcode.Split(':')[1])));
            else if (type == typeof(Operation))
                Messenger.Default.Send(new OperationBarcodeMessage(new KeyValuePair<Product, decimal>(_productRepository.GetByGuid(barcode.Split(':')[1]), decimal.Parse(barcode.Split(':')[2]))));
        }
    }

    public interface IProductsService
    {
        List<ProductListModel> GetCountedProducts(Category category);

        decimal GetProductCount(Product product);

        Product GetProduct(string guid);

        void Deposit(int productId, decimal quantity, Person person, DateTime? time = null);

        void Deposit(Product product, decimal quantity, Person person, DateTime? time = null);

        void Withdraw(int productId, decimal quantity, Person person, DateTime? time = null);

        void Withdraw(Product product, decimal quantity, Person person, DateTime? time = null);

        Type GetBarcodeType(string barcode);

        void ParseBarcode(string barcode);
    }
}