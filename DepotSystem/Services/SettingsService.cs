﻿using System;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;
using DepotSystem.Database;

namespace DepotSystem.Services
{
    public interface ISettingsService
    {
        string ComPort { get; set; }
        string Printer { get; set; }
    }

    public class SettingsService : ISettingsService
    {
        public string ComPort
        {
            get => GetSetting<string>(nameof(Settings.ReaderComPort));
            set => SetSetting(nameof(Settings.ReaderComPort), value);
        }

        public string Printer
        {
            get => GetSetting<string>(nameof(Settings.PrinterName));
            set => SetSetting(nameof(Settings.PrinterName), value);
        }

        private T GetSetting<T>(string name)
        {
            using (var db = new DepotSystemDbContext())
            {
                try
                {
                    return (T)typeof(Settings).GetProperty(name)?.GetValue(db.Settings.SingleOrDefault(s => s.Id == 1));
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                    return default;
                }
            }
        }

        private void SetSetting<T>(string name, T value)
        {
            using (var db = new DepotSystemDbContext())
            {
                try
                {
                    Settings setting;
                    if (!db.Settings.Any())
                        setting = db.Settings.Create();
                    else
                        setting = db.Settings.SingleOrDefault(s => s.Id == 1);

                    if (value != null)
                        setting?.GetType()?.GetProperty(name)?.SetValue(setting, value);

                    db.Settings.AddOrUpdate(setting);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                }
            }
        }

        public SettingsService()
        {
            try
            {
                using (var db = new DepotSystemDbContext())
                {
                    db.Database.Initialize(false);
                    Debug.WriteLine($"db: {db.Database.Connection.ConnectionString}");
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Could not initialize db: {e.Message}");
                throw;
            }
        }
    }
}