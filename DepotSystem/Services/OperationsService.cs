﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using DepotSystem.Database;
using DepotSystem.Models;
using FabSystems.Shared.Interfaces;

namespace DepotSystem.Services
{
    public class OperationsService : IOperationsService
    {
        public OperationsService() { }

        public List<LogListModel> GetLogs(int lastCount = 100)
        {
            List<LogListModel> result = new List<LogListModel>();

            try
            {
                using var db = new DepotSystemDbContext();

                var operations = db.Operations
                    .OrderByDescending(o => o.Date)
                    .Take(lastCount)
                    .Include(o => o.Person)
                    .Include(o => o.Product)
                    .ToList();

                foreach (var operation in operations)
                {
                    result.Add(new LogListModel()
                    {
                        Id = operation.Id,
                        PersonName = string.IsNullOrWhiteSpace(operation.Person.LastName) ? operation.Person.FirstName : $"{operation.Person.LastName} {operation.Person.FirstName}",
                        Count = operation.Quantity,
                        ProdcutName = operation.Product.Name,
                        Timestamp = operation.Date
                    });
                }

                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }
    }

    public interface IOperationsService
    {
        List<LogListModel> GetLogs(int lastCount = 100);
    }
}