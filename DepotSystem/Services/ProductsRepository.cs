﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using DepotSystem.Database;
using FabSystems.Shared.Interfaces;

namespace DepotSystem.Services
{
    public class ProductsRepository : IRepository<Product>
    {
        public IEnumerable<Product> GetAll()
        {
            using var db = new DepotSystemDbContext();
            IEnumerable<Product> result = db.Products.Where(p => p.Active).ToList().OrderBy(p => p.Name);
            return result;
        }

        public Product GetByGuid(string guid)
        {
            using var db = new DepotSystemDbContext();
            var result = db.Products.SingleOrDefault(p => p.Guid == guid);
            return result;
        }

        public Product GetById(int id)
        {
            using var db = new DepotSystemDbContext();
            var result = db.Products.SingleOrDefault(p => p.Id == id);
            return result;
        }

        public void Update(Product element)
        {
            using var db = new DepotSystemDbContext();
            db.Products.AddOrUpdate(element);
            db.SaveChanges();
        }

        public bool Delete(Product element)
        {
            element.Active = false;
            using var db = new DepotSystemDbContext();
            db.Products.AddOrUpdate(element);
            db.SaveChanges();
            return true;
        }

        public Product Create()
        {
            using var db = new DepotSystemDbContext();
            var result = db.Products.Create();
            result.Active = true;
            result.Guid = Guid.NewGuid().ToString().Replace("-", "");
            db.SaveChanges();
            return result;
        }
    }
}