﻿using DepotSystem.Database;
using GalaSoft.MvvmLight.Messaging;

namespace DepotSystem.Models
{
    public class ProductBarcodeMessage : GenericMessage<Product>
    {
        public ProductBarcodeMessage(Product content) : base(content)
        {
        }

        public ProductBarcodeMessage(object sender, Product content) : base(sender, content)
        {
        }

        public ProductBarcodeMessage(object sender, object target, Product content) : base(sender, target, content)
        {
        }
    }
}