﻿namespace DepotSystem.Models
{
    public class ProductListModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Count { get; set; }
        public string Guid { get; set; }
    }
}