﻿using System;

namespace DepotSystem.Models
{
    public class OperationException : Exception
    {
        public OperationException() : base () { }
        public OperationException(string message) : base(message) { }
    }
}