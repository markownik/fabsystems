﻿using System.Collections.Generic;
using DepotSystem.Database;
using GalaSoft.MvvmLight.Messaging;

namespace DepotSystem.Models
{
    public class OperationBarcodeMessage : GenericMessage<KeyValuePair<Product, decimal>>
    {
        public OperationBarcodeMessage(KeyValuePair<Product, decimal> content) : base(content)
        {
        }

        public OperationBarcodeMessage(object sender, KeyValuePair<Product, decimal> content) : base(sender, content)
        {
        }

        public OperationBarcodeMessage(object sender, object target, KeyValuePair<Product, decimal> content) : base(sender, target, content)
        {
        }
    }
}