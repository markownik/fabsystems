﻿using DepotSystem.Database;
using GalaSoft.MvvmLight.Messaging;

namespace DepotSystem.Models
{
    public class PersonalBarcodeMessage : GenericMessage<Person>
    {
        public PersonalBarcodeMessage(Person content) : base(content)
        {
        }

        public PersonalBarcodeMessage(object sender, Person content) : base(sender, content)
        {
        }

        public PersonalBarcodeMessage(object sender, object target, Person content) : base(sender, target, content)
        {
        }
    }
}