﻿using System;

namespace DepotSystem.Models
{
    public class LogListModel
    {
        public int Id { get; set; }

        public string ProdcutName { get; set; }

        public decimal Count { get; set; }

        public string PersonName { get; set; }

        public DateTime Timestamp { get; set; }
    }
}