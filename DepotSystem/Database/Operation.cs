﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SQLite.CodeFirst;

namespace DepotSystem.Database
{
    [Table("operations")]
    public class Operation
    {
        [Column("id"), Key, Index, Unique, Autoincrement]
        public int Id { get; set; }

        [Column("person_id"), ForeignKey(nameof(Person))]
        public int PersonId { get; set; }
        public Person Person { get; set; }

        [Column("product_id"), ForeignKey(nameof(Product))]
        public int ProductId { get; set; }
        public Product Product { get; set; }
        
        [Column("quantity")]
        public decimal Quantity { get; set; }

        [Column("date")]
        public DateTime Date { get; set; }
    }
}