﻿using System.Data.Entity;
using SQLite.CodeFirst;

namespace DepotSystem.Database
{
    public class DepotSystemDbContext : DbContext
    {
        public DepotSystemDbContext() : base("VSDB") { }

        public DepotSystemDbContext(string dbConnectionString) : base(dbConnectionString) { }

        public DbSet<Person> People { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Operation> Operations { get; set; }

        public DbSet<Settings> Settings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            System.Data.Entity.Database.SetInitializer(new SqliteCreateDatabaseIfNotExists<DepotSystemDbContext>(modelBuilder));
        }
    }
}