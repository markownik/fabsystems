﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SQLite.CodeFirst;
using Xceed.Wpf.Toolkit.Core.Attributes;

namespace DepotSystem.Database
{
    [Table("products")]
    public class Product : IProduct
    {
        [Column("id"), Key, Index, Unique, Autoincrement]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("type")]
        public Precision Type { get; set; }

        [Column("category")]
        public Category Category { get; set; }

        [Column("uid")]
        public string Guid { get; set; }

        [Column("active")]
        public bool Active { get; set; }
        
        public ICollection<Operation> Operations { get; set; }

        public void Load(IProduct value)
        {
            Name = value.Name;
            Type = value.Type;
            Category = value.Category;
            Active = value.Active;
        }
    }

    public enum Precision
    {
        [Description("1")]
        Full = 0,

        [Description("0,5")]
        Half = 2,
    }

    public enum Category
    {
        [Description("Magazyn")]
        Warehouse = 0,

        [Description("Kosmetyki")]
        Cosmetics = 1,

        [Description("Kosmetyki #2")]
        Cosmetics2 = 2
    }

    public interface IProduct
    {
        string Name { get; set; }

        Precision Type { get; set; }

        Category Category { get; set; }

        string Guid { get; set; }

        bool Active { get; set; }
    }
}