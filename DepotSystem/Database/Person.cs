﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SQLite.CodeFirst;

namespace DepotSystem.Database
{
    [Table("people")]
    public class Person : IPerson
    {
        [Column("id"), Key, Index, Unique, Autoincrement]
        public int Id { get; set; }

        [Column("first_name")]
        public string FirstName { get; set; }

        [Column("last_name")]
        public string LastName { get; set; }

        [Column("uid")]
        public string Guid { get; set; }

        [Column("active")]
        public bool Active { get; set; }

        public ICollection<Operation> Operations { get; set; }

        public void Load(IPerson value)
        {
            FirstName = value.FirstName;
            LastName = value.LastName;
            Active = value.Active;
        }

        public override string ToString()
        {
            return $"{LastName} {FirstName}";
        }
    }

    public interface IPerson
    {
        string FirstName { get; set; }

        string LastName { get; set; }

        string Guid { get; set; }

        bool Active { get; set; }
    }
}