﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SQLite.CodeFirst;

namespace DepotSystem.Database
{
    [Table("settings")]
    public class Settings
    {
        [Column("id"), Key, Unique]
        public int Id { get; set; }

        [Column("reader_com_port")]
        public string ReaderComPort { get; set; }

        [Column("printer_name")]
        public string PrinterName { get; set; }
    }
}