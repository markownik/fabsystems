﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FabSystems.Shared.Mvvm;

namespace DepotSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BaseVm ViewModel => (DataContext as BaseVm);

        public MainWindow()
        {
            InitializeComponent();
            ViewModel.CurrentWindow = this;

            Debug.WriteLine($"Current culture: {CultureInfo.CurrentCulture.Name}");
        }
    }
}
