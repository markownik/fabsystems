﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FabSystems.Shared.Mvvm;

namespace DepotSystem.Windows
{
    /// <summary>
    /// Interaction logic for UpdatePersonWindow.xaml
    /// </summary>
    public partial class UpdateProductWindow : Window
    {
        private BaseVm ViewModel => (DataContext as BaseVm);

        public UpdateProductWindow()
        {
            InitializeComponent();
            ViewModel.CurrentWindow = this;
        }
    }
}
