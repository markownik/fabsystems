﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FabSystems.Shared.Mvvm;

namespace DepotSystem.Windows
{
    /// <summary>
    /// Interaction logic for PeopleManagerWindow.xaml
    /// </summary>
    public partial class PeopleManagerWindow : Window
    {
        private BaseVm ViewModel => (DataContext as BaseVm);

        public PeopleManagerWindow()
        {
            InitializeComponent();
            ViewModel.CurrentWindow = this;
        }
    }
}
