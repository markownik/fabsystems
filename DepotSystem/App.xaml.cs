﻿using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Markup;
using GalaSoft.MvvmLight.Threading;

namespace DepotSystem
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            Xceed.Wpf.Toolkit.Licenser.LicenseKey = "WTK99-F0FG0-MHDY1-39RA";
            Xceed.Wpf.Themes.Windows10.Licenser.LicenseKey = "XPT99-7H9PR-1H1RH-R90A";
            //Xceed.Words.NET.Licenser.LicenseKey = "WDN99-709S0-4HPHW-300A";
            //Xceed.PDFCreator.NET.Licenser.LicenseKey = "PDF99-LW1SG-48LHH-09FA";
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("NjYyOTIzQDMyMzAyZTMxMmUzMFZyWSs5WTYxcVhVMm1yTTkraWJibHVBMUpmNStqWG5QTDJDZG9mR1h1aWM9");

            var culture = new CultureInfo("pl-PL");

            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;
            CultureInfo.CurrentCulture = culture;
            CultureInfo.CurrentUICulture = culture;

            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            DispatcherHelper.Initialize();

            base.OnStartup(e);
        }
    }
}
